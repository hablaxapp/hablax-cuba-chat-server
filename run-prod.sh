#!/bin/sh

forever stop chatprod
rm -rf /root/.forever/chatprod.log.previous
mv /root/.forever/chatprod.log chatprod.log.previous
forever --uid chatprod start app.js --prod
forever list
tail -f /root/.forever/chatprod.log