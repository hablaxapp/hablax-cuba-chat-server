#!/bin/sh

forever stop chatdev
rm -rf /root/.forever/chatdev.log.previous
mv /root/.forever/chatdev.log chatdev.log.previous
forever --uid chatdev start app.js --port 2345
forever list
tail -f /root/.forever/chatdev.log