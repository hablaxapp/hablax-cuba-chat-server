/**
 * Cuba_chat.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  connection: 'hablax_chat',
  tableName: 'message_custom',

  attributes: {

  	email_user_from: {
  		type: 'string',
  	},
      room_id: {
          type: 'integer',
          required: true
      },
  	email_user_to: {
  		type: 'string',
  	},
  	text_message: {
  		type: 'string',
  	},
  	admin_notes: {
  		type: 'string',
  	},
  	createdAt: {
  		type: 'string',
  	},
  	updatedAt: {
  		type: 'string',
  	},
	  room_id: {
		  type: 'integer'
	  },count: {
          type: 'integer'
      }

  }
};

