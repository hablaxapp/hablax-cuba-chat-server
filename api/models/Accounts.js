/**
 * Accounts.js
 *
 * @description :: The Accounts table
 * @docs        :: http://sailsjs.org/#!documentation/models
 */

module.exports = {
  connection: 'hablax_main',
  tableName: 'accounts',
  autoCreatedAt: false,
  autoUpdatedAt: false,
  attributes: {
    id: {
      type: 'integer',
      required: true,
      autoIncrement: true,
      primaryKey: true,
      size: 11
    },
    number: {
      type: 'string',
      required: true,
      index: true,
      size: 20
    },
    verified: {
      type: 'integer',
      required: true,
      size: 1,
      defaultsTo: '0'
    },
    reseller_id: {
      type: 'integer',
      required: false,
      index: true,
      size: 4,
      defaultsTo: null
    },
    pricelist_id: {
      type: 'integer',
      required: true,
      index: true,
      size: 4
    },
    status: {
      type: 'integer',
      required: true,
      size: 1,
      defaultsTo: '1'
    },
    credit: {
      type: 'decimal',
      required: true,
      defaultsTo: '0.00000'
    },
    sweep_id: {
      type: 'integer',
      required: true,
      size: 1,
      defaultsTo: '0'
    },
    creation: {
      type: 'datetime',
      required: true,
      defaultsTo: '0000-00-00 00:00:00'
    },
    credit_limit: {
      type: 'decimal',
      required: true,
      defaultsTo: '0.00000'
    },
    posttoexternal: {
      type: 'integer',
      required: true,
      size: 1,
      defaultsTo: '0'
    },
    balance: {
      type: 'decimal',
      required: true,
      defaultsTo: '0.00000'
    },
    password: {
      type: 'string',
      required: true,
      size: 20
    },
    first_name: {
      type: 'string',
      required: true,
      size: 40
    },
    last_name: {
      type: 'string',
      required: true,
      size: 40
    },
    company_name: {
      type: 'string',
      required: true,
      size: 40
    },
    address_1: {
      type: 'string',
      required: true,
      size: 80
    },
    address_2: {
      type: 'string',
      required: true,
      size: 80
    },
    postal_code: {
      type: 'string',
      required: true,
      size: 12
    },
    province: {
      type: 'string',
      required: true,
      size: 20
    },
    city: {
      type: 'string',
      required: true,
      size: 20
    },
    country_id: {
      type: 'integer',
      required: true,
      size: 3,
      defaultsTo: '0',
      model: 'transferto_countries'
    },
    telephone_1: {
      type: 'string',
      required: true,
      size: 20
    },
    telephone_2: {
      type: 'string',
      required: true,
      size: 20
    },
    email: {
      type: 'string',
      required: true,
      size: 80
    },
    language_id: {
      type: 'integer',
      required: true,
      size: 3,
      defaultsTo: '0'
    },
    currency_id: {
      type: 'integer',
      required: true,
      size: 3,
      defaultsTo: '0'
    },
    maxchannels: {
      type: 'integer',
      required: true,
      size: 4,
      defaultsTo: '1'
    },
    dialed_modify: {
      type: 'mediumtext',
      required: true
    },
    type: {
      type: 'integer',
      required: false,
      size: 1,
      defaultsTo: '0'
    },
    timezone_id: {
      type: 'integer',
      required: true,
      size: 3,
      defaultsTo: '0'
    },
    inuse: {
      type: 'integer',
      required: true,
      size: 4,
      defaultsTo: '0'
    },
    deleted: {
      type: 'integer',
      required: true,
      size: 1,
      defaultsTo: '0'
    },
    notify_credit_limit: {
      type: 'integer',
      required: true,
      size: 11
    },
    notify_flag: {
      type: 'integer',
      required: true,
      size: 1
    },
    notify_email: {
      type: 'string',
      required: true,
      size: 80
    },
    commission_rate: {
      type: 'integer',
      required: true,
      size: 11,
      defaultsTo: '0'
    },
    invoice_day: {
      type: 'integer',
      required: true,
      size: 11,
      defaultsTo: '0'
    },
    pin: {
      type: 'string',
      required: true,
      size: 20
    },
    first_used: {
      type: 'datetime',
      required: true
    },
    expiry: {
      type: 'datetime',
      required: true
    },
    validfordays: {
      type: 'integer',
      required: true,
      size: 7
    },
    remember_token: {
      type: 'string',
      required: false,
      size: 100,
      defaultsTo: null
    },
    country: {
      type: 'string',
      required: true,
      size: 20
    },
    iso_code: {
      type: 'string',
      required: true,
      size: 3
    },
    country_code: {
      type: 'integer',
      required: true,
      size: 4,
      defaultsTo: '1'
    },
    first_ip: {
      type: 'string',
      required: false,
      size: 16,
      defaultsTo: null
    },
    registered_language: {
      type: 'string',
      required: false,
      size: 2,
      defaultsTo: null
    },
    selected_language: {
      type: 'string',
      required: false,
      size: 2,
      defaultsTo: null
    },
    currency: {
      type: 'string',
      required: true,
      size: 5,
      defaultsTo: 'USD'
    },
    segmento: {
      type: 'string',
      required: false,
      size: 60,
      defaultsTo: null
    },
    like_iso_code_country: {
      type: 'string',
      required: false,
      size: 3,
      defaultsTo: null
    },
    referer: {
      type: 'string',
      required: false,
      size: 30,
      defaultsTo: null
    }
  }
};