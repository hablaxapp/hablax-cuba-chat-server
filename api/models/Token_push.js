/**
 * Token_push.js
 *
 * @description :: The Token_push table
 * @docs        :: http://sailsjs.org/#!documentation/models
 */

module.exports = {
  connection: 'hablax_main',
  tableName: 'token_push',
  autoCreatedAt: false,
  autoUpdatedAt: false,
  attributes: {
    id: {
      type: 'integer',
      required: true,
      autoIncrement: true,
      primaryKey: true,
      size: 11
    },
    pushToken: {
      type: 'text',
      required: true
    },
    devicePlatform: {
      type: 'text',
      required: true
    },
    deviceModel: {
      type: 'text',
      required: true
    },
    deviceVersion: {
      type: 'text',
      required: true
    },
    deviceUUID: {
      type: 'text',
      required: true
    },
    account_id: {
      type: 'integer',
      required: true,
      size: 11,
      model: 'accounts'
    }
  }
};