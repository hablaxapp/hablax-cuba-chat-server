/**
 * Cuba_chat.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  connection: 'hablax_chat',
  tableName: 'messages',

  attributes: {

  	email_user_from: {
  		type: 'string'
  	},visible_with_user: {
  		type: 'text'
  	},
  	email_user_to: {
  		type: 'string'
  	},
  	text_message: {
  		type: 'string'
  	},
  	admin_notes: {
  		type: 'string'
  	},
  	createdAt: {
  		type: 'string'
  	},
  	updatedAt: {
  		type: 'string'
  	},
      track_status: {
  		type: 'integer'
  	},
    order_in_room: {
  	    type: 'integer'
    },

	  room_id: {
		  type: 'integer'
	  },id:{
          type: 'integer',
          required: true,
          autoIncrement: true,
          primaryKey: true,
          size: 11
	  }

  }
};

