/**
 * Cuba_chat.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

    connection: 'hablax_chat',
    tableName: 'server_email',
    autoCreatedAt: false,
    autoUpdatedAt: false,
    attributes: {


        id: {
            type: 'integer',
            required: true,
            autoIncrement: true,
            primaryKey: true,
            size: 11
        },
        email: {
            type: 'text'
        }


    }
};

