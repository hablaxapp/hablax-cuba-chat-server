moment = require("moment");

/**
 * Accounts.js
 *
 * @description :: The Accounts table
 * @docs        :: http://sailsjs.org/#!documentation/models
 */

module.exports = {
  connection: 'hablax_chat',
  tableName: 'cuba_accounts',
  autoCreatedAt: false,
  autoUpdatedAt: false,
  attributes: {
    id: {
      type: 'integer',
      autoIncrement: true,
      primaryKey: true,
      size: 11
    },
    number: {
      type: 'string',
      required: true,
      index: true,
      size: 20
    },
    creation: {
        type: 'string',
        required: true,
        size: 40
    },
    email: {
      type: 'string',
      required: true,
      size: 80
    },
    deleted: {
      type: 'integer',
      required: true,
      size: 1,
      defaultsTo: '0'
    },
    iso_code: {
      type: 'string',
      required: true,
      size: 3
    },


    app_version: {
      type: 'string',
      required: true,
      size: 15
    },
    device_platform: {
      type: 'string',
      required: true,
      size: 20
    },
    device_model: {
      type: 'string',
      required: true,
      size: 50
    },
    device_version: {
      type: 'string',
      required: true,
      size: 20
    },
    device_uuid: {
      type: 'string',
      required: true,
      size: 40
    },
    update_at: {
      type: 'string',
      required: true,
        size: 40
    }
  }
};