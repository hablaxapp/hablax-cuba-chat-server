/**
 * Accounts.js
 *
 * @description :: The Accounts table
 * @docs        :: http://sailsjs.org/#!documentation/models
 */

module.exports = {
  connection: 'hablax_main',
  tableName: 'accounts_contacts',
  autoCreatedAt: false,
  autoUpdatedAt: false,
  attributes: {
    id: {
      type: 'integer',
      required: true,
      autoIncrement: true,
      primaryKey: true,
      size: 11
    },
    accountid: {
      type: 'integer',
      required: false,
      index: true,
      size: 11,
      defaultsTo: null
    },
    accountnumber: {
      type: 'string',
      required: false,
      index: true,
      size: 20
    },
    contact_position: {
      type: 'integer',
      required: true,
      size: 11
    },
    contact_name: {
      type: 'string',
      required: false,
      index: true,
      size: 20
    },
    contact_number: {
      type: 'string',
      required: false,
      index: true,
      size: 20
    },
    status: {
      type: 'integer',
      required: true,
      size: 1
    },
    contact_email: {
      type: 'string',
      required: true,
      size: 20
    },
    country_iso: {
      type: 'string',
      required: true,
      size: 2
    }


  }
};