/**
 * Accounts.js
 *
 * @description :: The Accounts table
 * @docs        :: http://sailsjs.org/#!documentation/models
 */

module.exports = {
  connection: 'hablax_main',
  tableName: 'app_contacts',
  autoCreatedAt: false,
  autoUpdatedAt: false,
  attributes: {
      row_id: {
      type: 'integer',
      required: true,
      autoIncrement: true,
      primaryKey: true,
      size: 11
    },
      account_id: {
      type: 'integer',
      required: true,
      size: 11
    },
      contact_name: {
      type: 'string',
      required: true,
      size: 50
    },
      contact_phone: {
      type: 'string',
      required: true,
      size: 20
    },
      contact_email: {
      type: 'string',
      required: true,
      size: 30
    },
      contact_country_iso: {
      type: 'string',
      required: true,
      size: 2
    },
      status: {
      type: 'integer',
      required: true,
      size: 1
    }

  }
};