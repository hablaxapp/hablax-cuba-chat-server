/**
 * Transferto_countries.js
 *
 * @description :: The Transferto_countries table
 * @docs        :: http://sailsjs.org/#!documentation/models
 */

module.exports = {
  connection: 'hablax_main',
  
  tableName: 'transferto_countries',
  autoCreatedAt: false,
  autoUpdatedAt: false,
  attributes: {
    id: {
      type: 'integer',
      required: true,
      autoIncrement: true,
      primaryKey: true,
      size: 11
    },
    tt_countryid: {
      type: 'integer',
      required: true,
      unique: true,
      size: 11
    },
    priority: {
      type: 'integer',
      required: true,
      size: 1,
      defaultsTo: '0'
    },
    country: {
      type: 'string',
      required: true,
      unique: true,
      size: 60
    },
    country_es: {
      type: 'string',
      required: false,
      size: 60,
      defaultsTo: null
    },
    iso: {
      type: 'string',
      required: true,
      size: 2
    },
    country_code: {
      type: 'string',
      required: true,
      size: 7
    },
    mobile_starts_with: {
      type: 'string',
      required: true,
      size: 11
    },
    validate_in: {
      type: 'string',
      required: true,
      size: 10,
      defaultsTo: 'api'
    },
    enabled: {
      type: 'integer',
      required: true,
      size: 1,
      defaultsTo: '1'
    }
  }
};