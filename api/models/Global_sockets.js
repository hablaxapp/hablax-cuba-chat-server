/**
 * Accounts.js
 *
 * @description :: The Accounts table
 * @docs        :: http://sailsjs.org/#!documentation/models
 */

module.exports = {
  connection: 'hablax_chat',
  tableName: 'global_sockets',
  autoCreatedAt: false,
  autoUpdatedAt: false,
  attributes: {
    id: {
      type: 'integer',
      required: true,
      autoIncrement: true,
      primaryKey: true,
      size: 11
    },
    socket_id: {
      type: 'string',
      required: false,
      size: 20
    },
    global_account_email: {
      type: 'string',
      required: true,
      size: 20
    },
    global_account_token: {
      type: 'string',
      required: true,
      size: 255
    }


  }
};