/**
 * Cuba_chat.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

    connection: 'hablax_chat',
    tableName: 'rooms',
    autoCreatedAt: false,
    autoUpdatedAt: false,
    attributes: {

        room_members: {
            type: 'text'
        },
        id: {
            type: 'integer',
            required: true,
            autoIncrement: true,
            primaryKey: true,
            size: 11
        },
        phone_numbers: {
            type: 'text'
        },
        member_names: {
            type: 'text'
        },
        recent_message: {
            type: 'text'
        }
        , country_code: {
            type: 'text'
        }, pending: {
            type: 'integer'
        }
        , country_code_iso: {
            type: 'text'
        }
        , send_outside: {
            type: 'integer'
        }
        , createdAt: {
            type: 'text'
        },
        time_update: {
            type: 'string'
        },
        visible_with_user: {
            type: 'text'
        }


    }
};

