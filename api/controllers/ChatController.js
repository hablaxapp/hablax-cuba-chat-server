var chatService = require('../services/ChatService.js');
var hablaxMainService = require('../services/HablaxMainService.js');
var emailService = require('../services/EmailService.js');

/**
 * ChatController
 *
 * @description :: Server-side logic for managing chats
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

    sendChatMessage1: function (req, res) {
        var senderEmail = req.query.senderEmail;
        var receiptPhoneNumber = req.query.receiptPhoneNumber;
        var textMessage = req.query.textMessage;
        var senderPhoneNumber = req.query.senderPhoneNumber;
        var senderCountryCode = req.query.senderCountryCode;
        var senderName = req.query.senderName;
        var receiptCountryIso = req.query.receiptCountryIso;
        var receiptCountryCode = req.query.receiptCountryCode;
        var senderCountryIso = req.query.senderCountryIso;
        var senderId = req.query.senderId;
        var token = req.query.token;

        console.log("token "+token);
        if (req.file('imageFile') != null) {
            req.file('imageFile').upload({
                // don't allow the total upload size to exceed ~10MB
                maxBytes: 10000000
            },function whenDone(err, uploadedFiles) {
                if (err) {

                    return res.negotiate(err);
                }

                console.log('Has image file');

                //chatService.globalSendChatMessage(senderId, senderEmail, senderName, senderCountryCode, senderCountryIso,
                //    receiptPhoneNumber, receiptCountryCode, receiptCountryIso, textMessage, senderPhoneNumber, token, imageFile,
                //    function(response) {
                //        res.json(response);
                //
                //    }, function(error) {
                //        res.json("ERROR");
                //    });
            });
        } else {
            console.log('No image file');
            //chatService.globalSendChatMessage(senderId, senderEmail, senderName, senderCountryCode, senderCountryIso,
            //    receiptPhoneNumber, receiptCountryCode, receiptCountryIso, textMessage, senderPhoneNumber, token, imageFile,
            //    function(response) {
            //        res.json(response);
            //
            //    }, function(error) {
            //        res.json("ERROR");
            //    });
        }
    },

    sendChatMessage: function (req, res) {
        var senderEmail = req.query.senderEmail;
        var receiptPhoneNumber = req.query.receiptPhoneNumber;
        var textMessage = req.query.textMessage;
        var senderName = req.query.senderName;
        var receiptCountryIso = req.query.receiptCountryIso;
        var receiptCountryCode = req.query.receiptCountryCode;
        var senderPhoneNumber = req.query.senderPhoneNumber;
        var senderCountryIso = req.query.senderCountryIso;
        var senderCountryCode = req.query.senderCountryCode;
        var senderId = req.query.senderId;
        var token = req.query.token;


        if (senderEmail == null || senderEmail == '') {
            res.json("ERROR");
            return;
        }
        if (receiptPhoneNumber == null || receiptPhoneNumber == '') {
            res.json("ERROR");
            return;
        }
        if (textMessage == null || textMessage == '') {
            res.json("ERROR");
            return;
        }
        if (senderName == null || senderName == '') {
            res.json("ERROR");
            return;
        }
        if (receiptCountryIso == null || receiptCountryIso == '') {
            res.json("ERROR");
            return;
        }
        if (receiptCountryCode == null || receiptCountryCode == '') {
            res.json("ERROR");
            return;
        }
        if (senderPhoneNumber == null || senderPhoneNumber == '') {
            res.json("ERROR");
            return;
        }
        if (senderCountryCode == null || senderCountryCode == '') {
            res.json("ERROR");
            return;
        }
        if (senderCountryIso == null || senderCountryIso == '') {
            res.json("ERROR");
            return;
        }
        if (senderId == null || senderId == '') {
            res.json("ERROR");
            return;
        }
        if (token == null || token == '') {
            res.json("ERROR");
            return;
        }

        chatService.globalSendChatMessage(senderId, senderEmail, senderName, senderCountryCode, senderCountryIso,
                    receiptPhoneNumber, receiptCountryCode, receiptCountryIso, textMessage, senderPhoneNumber, token,
                    function(response) {
            console.log("RESPONSE TO MOBILE APP");
            res.json(response);
        }, function(error) {
            res.json("ERROR");
        });


    },
    fetchAllChatRooms: function (req, res) {


        var myEmail = req.query.myEmail;
        console.log(myEmail);
        chatService.globalFetchAllChatRoom(myEmail, function(response) {
            res.json(response);
        }, function(error) {
            res.json("ERROR");
        });


    },
    fetchAllUnseenChatRoom: function (req, res) {

        var from = req.query.from;
        var email = req.query.email;
        console.log(from);
        console.log(email);
        chatService.globalFetchUnseenChatRoom(from, email, function(response) {
            res.json(response);
        }, function(error) {
            res.json("ERROR");
        });


    },
    deleteChatRoomById: function (req, res) {

        var roomId = req.query.roomId;
        var phoneNumberDeleted = req.query.phoneNumberDeleted;
        chatService.globalDelRoomById(roomId,phoneNumberDeleted, function(response) {
            res.json(response);
        }, function(error) {
            res.json("ERROR");
        });


    },
    updateMessageStatus: function (req, res) {

        var roomId = req.query.roomId;
        var email_from = req.query.email_from;
        var email_to = req.query.email_to;
        var iso_from = req.query.iso_from;
        var phone_number_from = req.query.phone_number_from;
        var phone_number_to = req.query.phone_number_to;
        chatService.updateMessageStatus(roomId,email_from,email_to,iso_from,phone_number_from,phone_number_to, function(response) {
            res.json(response);
        }, function(error) {
            res.json("ERROR");
        });


    },
    stopSocketChatConnection: function (req, res) {

        var token = req.query.token;
        chatService.stopSocketChatConnection(token, function(response) {
            res.json(response);
        }, function(error) {
            res.json("ERROR");
        });


    },
    resendChatMessage: function (req, res) {

        var messageRowId = req.query.messageRowId;
        var roomId = req.query.roomId;
        var text = req.query.text;
        var senderPhoneNumber = req.query.senderPhoneNumber;
        var receiptPhoneNumber = req.query.receiptPhoneNumber;
        var receiptCountryCode = req.query.receiptCountryCode;
        var receiptCountryIso = req.query.receiptCountryIso;
        var token = req.query.token;
        var senderId = req.query.senderId;
        var senderName = req.query.senderName;
        var senderCountryCode = req.query.senderCountryCode;
        var senderCountryIso = req.query.senderCountryIso;
        var senderEmail = req.query.senderEmail;

        chatService.resendChatMessage(messageRowId,roomId,text,senderPhoneNumber,receiptPhoneNumber,
                                receiptCountryCode,receiptCountryIso,token,senderId,senderName,
                                senderCountryCode,senderCountryIso,senderEmail, function(response) {
            res.json(response);
        }, function(error) {
            res.json("ERROR");
        });


    },
    unlockChatMessage: function (req, res) {

        var messageRowId = req.query.messageRowId;
        var roomId = req.query.roomId;
        var token = req.query.token;
        var accountId = req.query.accountId;
        chatService.unlockChatMessage(accountId, messageRowId,roomId,token, function(response) {
            res.json(response);
        }, function(error) {
            res.json("ERROR");
        });


    },

    fetchAllMessages: function (req, res) {

        var roomId = req.query.roomId;
        // var partnerPhoneNumber = req.query.partnerPhoneNumber;
        // var partnerCountryIso = req.query.partnerCountryIso;

        chatService.globalFetchAllMessages(roomId, function(response) {
            res.json(response);
        }, function(error) {
            res.json("ERROR");
        });


    },fetchAllMessagesByPhoneNumber: function (req, res) {

        var phone_number_from = req.query.phone_number_from;
        var phone_number_to = req.query.phone_number_to;
        console.log("phone_number_from "+phone_number_from);
        console.log("phone_number_to "+phone_number_to);

        chatService.fetchAllMessagesByPhoneNumber(phone_number_from,phone_number_to, function(response) {
            res.json(response);
        }, function(error) {
            res.json("ERROR");
        });


    },fetchAllMessagesAndRoomByRoomID: function (req, res) {

        var room_id = req.query.room_id;

        chatService.fetchAllMessagesAndRoomByRoomID(room_id, function(response) {
            res.json(response);
        }, function(error) {
            res.json("ERROR");
        });


    },uploadImage : function(req, res){

        // var simpleObject = {};
        // for (var prop in req ){
        //     if (!req.hasOwnProperty(prop)){
        //         continue;
        //     }
        //     if (typeof(req[prop]) == 'object'){
        //         continue;
        //     }
        //     if (typeof(req[prop]) == 'function'){
        //         continue;
        //     }
        //     simpleObject[prop] = req[prop];
        // }
        // console.log(JSON.stringify(simpleObject));
        req.file('file').upload({
            // Directory path where you want to save...
            dirname : '/root/images/'

        },function(err, file){
            if(err) {
                console.log("err " + err);
                res.json("ERROR " + err);
                return;
            }
            res.json({file: file});
            var imgUrl = file[0].fd;


            emailService.sendSMTPCustom(imgUrl,
                function(res) {
                    // nothing
                    console.log(res);
                });

            console.log(imgUrl);
        });
    }




};

