/**
 * ChatsocketController
 *
 * @description :: Server-side logic for managing Chatsockets
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {


    globalSubscribe: function (req, res) {
        var SOCKET_MESSAGE = "ChatMessage";
        // Make sure this is a socket request (not traditional HTTP)
        if (!req.isSocket) {
            return res.badRequest();
        }

        var account_id = req.body.account_id;
        var account_token = req.body.account_token;
        var account_email = req.body.account_email;
        var socket_type = req.body.socket_type;//0 is chat socket,1 is signal socket
        var socketID = sails.sockets.getId(req);

        var socketName = "Socket_" + account_email;

        console.log("Socket name: " + socketName);
        console.log("account_token: " + account_token);
        console.log("account_email: " + account_email);
        console.log("socketID: " + socketID);




        Global_sockets.find({
            global_account_token: account_token
        }).where({}).exec(function (e1, socketList) {
            if(e1){
                console.log(e1);
                return;
            }

            if(socketList && socketList.length > 0) {

                for (var index = 0; index < socketList.length; index++) {
                    var rowId = socketList[index].id;
                    var oldSocketId = socketList[index].socket_id;

                    Global_sockets.destroy({
                        id: rowId
                    }).exec(function (err) {
                        if (err) {
                            console.log(err);
                            if (errorCB) {
                                errorCB(err);
                            }
                            return;
                        }

                        sails.sockets.leave(oldSocketId, oldSocketId, function (err1) { // this dont work
                            if (err1) {
                                console.log(err1);
                                if (errorCB) {
                                    errorCB(err1);
                                }
                                return;
                            }
                        });

                        sails.sockets.leaveAll(oldSocketId, function (err1) { // this dont work
                            if (err1) {
                                console.log(err1);
                                if (errorCB) {
                                    errorCB(err1);
                                }
                                return;
                            }

                        });

                    });


                }
            }


                Global_sockets.create({
                    id: 0,
                    socket_id: socketID,
                    global_account_email: account_email,
                    global_account_token: account_token
                }).exec(function (e, r) {

                    if (e) {
                        console.log(e);
                        return;
                    }
                    if (r) {
                        sails.sockets.join(req, socketID, function (err) {
                            if (err) {
                                return res.serverError(err);
                            }
                        });

                    }
                });


            //});





        });




        // Have the socket which made the request join the "funSockets" room.


        // ^^^
        // At this point, we've blasted out a socket message to all sockets who have
        // joined the "funSockets" room.  But that doesn't necessarily mean they
        // are _listening_.  In other words, to actually handle the socket message,
        // connected sockets need to be listening for this particular event (in this
        // case, we broadcasted our message with an event name of "hello").  The
        // client-side you'd need to write looks like this:
        //
        // io.socket.on('hello', function (broadcastedData){
        //   console.log(data.howdy);
        //   // => 'hi there!'
        // }
        //

        // Now that we've broadcasted our socket message, we still have to continue on
        // with any other logic we need to take care of in our action, and then send a
        // response.  In this case, we're just about wrapped up, so we'll continue on

        // Respond to the request with a 200 OK.
        // The data returned here is what we received back on the client as `data` in:
        // `io.socket.get('/say/hello', function gotResponse(data, jwRes) { /* ... */ });`
        return res.json({
            anyData: 'we want to send back'
        });
    }

};

