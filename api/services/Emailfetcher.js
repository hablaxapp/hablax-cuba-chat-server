var HablaxMainService = require('../services/HablaxMainService.js');
var emailService = require('../services/EmailService.js');
var chatService = require('../services/ChatService.js');

module.exports = {

    shouldRun: true,

    createListener2Instance: function () {

        var thiz = this;

        var mailListenerIns = new sails.listener2({
            username: "chatserver@hablax.com",
            password: "hablaxChat!",
            host: "siamopizza.com",
            port: 993, // imap port
            tls: true,
            connTimeout: 10000, // Default by node-imap
            authTimeout: 5000, // Default by node-imap,
            debug: console.log, // Or your custom function with only one incoming argument. Default: null
            tlsOptions: {rejectUnauthorized: false},
            mailbox: "INBOX", // mailbox to monitor
            searchFilter: ["UNDELETED"], // the search filter being used after an IDLE notification has been retrieved
            markSeen: true, // all fetched email willbe marked as seen and not fetched next time
            fetchUnreadOnStart: true, // use it only if you want to get all unread email on lib start. Default is `false`,
            mailParserOptions: {streamAttachments: false}, // options to be passed to mailParser lib.
            attachments: false, // download attachments as they are encountered to the project directory
            attachmentOptions: {directory: "attachments/", stream: "true"} // specify a download directory for attachments
        });

        mailListenerIns.on("server:connected", function(){
            console.log("imapConnected");
        });

        mailListenerIns.on("error", function(err){
            console.log(err);
        });

        mailListenerIns.on("attachment", function(attachment){
            console.log(attachment.path);
        });

        mailListenerIns.on("server:disconnected", function(){
            console.log("Disconnected");
            var mailListener = thiz.createListener2Instance();
            mailListener.start(); // start again if Session expired.


        });

        mailListenerIns.on("mail", function (mail, seqno, attributes) {
            //console.log(seqno);
            //console.log(attributes);
            //console.log(mail.subject);
            //return;

            // do something with mail object including attachments
            // console.log("emailParsed", mail);
            // console.log(mail.subject);
            // var newMessage = res.new_messages[index];

            // syntax: [hablax||category::command::json_parameters], e.i: hablax||chat::sendMessage::{}
            var values = mail.subject.split("||");
            console.log(values);
            if (values.length == 2) {
                if (values[0] == "hablax") {

                    // syntax: [category::command::json_parameters], e.i: chat::sendMessage::{}
                    var tempValues = values[1].split("::");

                    if (tempValues != null && tempValues.length == 3) {

                        if (tempValues[0] == "chat") {
                            if (tempValues[1] == "sendMessage") {
                                try {

                                    var parameters = JSON.parse(tempValues[2]);
                                    console.log(parameters);

                                    if (parameters.senderEmail != null
                                        && parameters.senderPhoneNumber != null
                                        && parameters.senderCountryCode != null
                                        && parameters.receiptCountryCode != null
                                        && parameters.senderCountryIso != null
                                        && parameters.receiptCountryIso != null
                                        && parameters.receiptPhoneNumber != null
                                        && parameters.textMessage != null
                                        && parameters.cubaMessageID != null) {
                                        console.log("Incoming text: "+parameters.textMessage);
                                        var account = null;
                                        var senderEmail = parameters.senderEmail;
                                        var receiptPhoneNumber = parameters.receiptPhoneNumber;
                                        var senderPhoneNumber = parameters.senderPhoneNumber;
                                        var senderCountryCode = parameters.senderCountryCode;
                                        var senderCountryIso = parameters.senderCountryIso;

                                        var receiptCountryCode = parameters.receiptCountryCode;
                                        var receiptCountryIso = parameters.receiptCountryIso;
                                        var textMessage = parameters.textMessage;
                                        var cubaMessageID = parameters.cubaMessageID;


                                        console.log("SMS message: " + textMessage);
                                        console.log("Receipt country ISO: " + receiptCountryIso);
                                        console.log("Receipt country code: " + receiptCountryCode);


                                        if((receiptCountryIso == null || receiptCountryIso == "null") && (receiptCountryCode == null || receiptCountryCode == "null")){

                                            Cuba_accounts.findOne({number: receiptPhoneNumber}).exec(function (err, cubaRecord) {

                                                if (err) {
                                                    console.log(err);
                                                    return;
                                                }
                                                if (cubaRecord){
                                                    console.log("Found Cuba account");
                                                    chatService.handle_message_from_cuba(parameters,"CU","53");
                                                } else {

                                                    Accounts_search_chat.findOne({
                                                        or : [
                                                            { formattedPhoneNumber: receiptPhoneNumber },
                                                            { telephone_1: receiptPhoneNumber }]
                                                    }).exec(function (err1, record) {

                                                        if (err1) {
                                                            console.log(err1);
                                                            return;
                                                        }
                                                        if(record){
                                                            console.log("Found international account");
                                                            chatService.handle_message_from_cuba(parameters,record.iso_code,record.country_code);
                                                        }else {
                                                            console.log("This number ("+receiptPhoneNumber+") doesn't register our Hablax app (included International & Cuba version).");
                                                            chatService.handle_message_from_cuba(parameters,"","");
                                                        }
                                                    });

                                                }
                                            });
                                        }else {
                                            chatService.handle_message_from_cuba(parameters,null,null);

                                        }





                                    }
                                } catch (e) {
                                    // nothing
                                    console.log(e);
                                }

                            }else if (tempValues[1] == "trackMessage") {
                                try {
                                    var SOCKET_MESSAGE = "ChatMessage";
                                    var SOCKET_SIGNAL = "ChatSignal";
                                    var parameters = JSON.parse(tempValues[2]);
                                    var phoneNumberFrom = parameters.phoneNumberFrom;
                                    var phoneNumberTo = parameters.phoneNumberTo;
                                    var myPhoneNumber = parameters.myPhoneNumber;
                                    console.log("phoneNumberFrom " + phoneNumberFrom);
                                    console.log("phoneNumberTo " + phoneNumberTo);

                                    Rooms.findOne().where({
                                        or: [
                                            { phone_numbers: { like: "%"+phoneNumberFrom+"%"+phoneNumberTo+"%" } },
                                            { phone_numbers: { like: "%"+phoneNumberTo+"%"+phoneNumberFrom+"%" } }
                                        ]

                                    }).exec(function (err1, record) {
                                        if (record) {
                                            console.log(record);
                                            var emailArray = record.room_members.split(",");
                                            var phoneArray = record.phone_numbers.split(",");
                                            var isoArray = record.country_code_iso.split(",");
                                            var emailFrom = '';
                                            var emailTo = '';
                                            var isoFrom = '';
                                            var isoTo = '';
                                            if(phoneNumberFrom == phoneArray[0]){
                                                emailFrom = emailArray[0];
                                                emailTo = emailArray[1];
                                                isoFrom = isoArray[0];
                                                isoTo = isoArray[1];
                                            }else {
                                                emailFrom = emailArray[1];
                                                emailTo = emailArray[0];
                                                isoFrom = isoArray[1];
                                                isoTo = isoArray[1];
                                            }
                                            console.log("emailArray "+JSON.stringify(emailArray));
                                            console.log("emailFrom "+emailFrom);
                                            console.log("emailTo "+emailTo);
                                            Messages.update({
                                                room_id: record.id,
                                                email_user_from:emailFrom,
                                                email_user_to:emailTo,
                                                or : [
                                                    { track_status: 11 },
                                                    { track_status: 13 }]
                                            },{ track_status: 12 } ).exec(function (e, r) {
                                                if(e){
                                                    console.log(e);
                                                    return;
                                                }
                                                console.log(r);
                                                if(r) {
                                                    if(isoFrom == "CU" || isoFrom == "cu"){
                                                        console.log("cuba socket in cuba page");
                                                        emailService.sendSMTP(emailFrom, "update",
                                                            'hablax||chat::trackMessageStatus::{"phone_number_from":"'+phoneNumberFrom+'","phone_number_to":"'+phoneNumberTo+'"}',
                                                            function(res) {
                                                                // nothing
                                                            });
                                                    }else {

                                                        var socketData = {
                                                            email_from: emailFrom,
                                                            room_id: record.id,
                                                            messages: r
                                                        };

                                                        chatService.sendSocketMessage(emailFrom, SOCKET_SIGNAL, socketData, function() {
                                                            // nothing
                                                        });


                                                    }

                                                }

                                            });
                                        }

                                    });


                                }catch (e){

                                }
                            }

                        } else if (tempValues[0] == "system") {

                            if (tempValues[1] == "register") { // register or login
                                try {
                                    var parameters = JSON.parse(tempValues[2]);
                                    if (parameters.nautaEmail != null && parameters.cubaPhoneNumber != null
                                        && parameters.appVersion != null && parameters.devicePlatform != null
                                        && parameters.deviceModel != null && parameters.deviceVersion != null
                                        && parameters.deviceUUID != null) {

                                        console.log(parameters);
                                        console.log("Find cuba account");

                                        Cuba_accounts.findOne({email: parameters.nautaEmail}).exec(function (err, record) {

                                            if (err) {
                                                console.log("Error in find one cuba account");
                                                console.log(err);
                                                return;
                                            }
                                            if (record == null) { // no found Cuba account
                                                Cuba_accounts.create({
                                                    email: parameters.nautaEmail,
                                                    number: parameters.cubaPhoneNumber,
                                                    iso_code: "CU",
                                                    country_code: "53",
                                                    app_version: parameters.appVersion,
                                                    device_platform: parameters.devicePlatform,
                                                    device_model: parameters.deviceModel,
                                                    device_version: parameters.deviceVersion,
                                                    device_uuid: parameters.deviceUUID,
                                                    creation: moment().format("YYYY-MM-DDTHH:mm:ss.SSSZ"),
                                                    update_at: moment().format("YYYY-MM-DDTHH:mm:ss.SSSZ")
                                                }).exec(function (e, r) {
                                                    if (e) {
                                                        console.log(e);
                                                    }
                                                    if (r) {
                                                        console.log(r);
                                                    }
                                                });
                                            } else { // existing Cuba account
                                                var oldNumber = record.number;
                                                var newNumber = parameters.cubaPhoneNumber;


                                                var now = moment().format("YYYY-MM-DDTHH:mm:ss.SSSZ");

                                                Cuba_accounts.update({ id: record.id } , {
                                                    email: parameters.nautaEmail,
                                                    number: parameters.cubaPhoneNumber,
                                                    app_version: parameters.appVersion,
                                                    device_platform: parameters.devicePlatform,
                                                    device_model: parameters.deviceModel,
                                                    device_version: parameters.deviceVersion,
                                                    device_uuid: parameters.deviceUUID,
                                                    update_at: now
                                                }).exec(function (e, r) {
                                                    console.log(e);
                                                    console.log(r);
                                                });

                                                Rooms.find().where({
                                                    phone_numbers: { like: "%"+oldNumber+"%" }
                                                }).exec(function (err1, record) {
                                                    console.log("ADAYROI");
                                                    console.log(record);

                                                    for(var index = 0; index < record.length; index++){
                                                        var obj = record[index];
                                                        var current = obj.phone_numbers;
                                                        var oldNumberArray = current.split(",");
                                                        var string = '';
                                                        if(oldNumberArray[0] == oldNumber){
                                                            string = newNumber+","+oldNumberArray[1];
                                                        }else if(oldNumberArray[1] == oldNumber){
                                                            string = oldNumberArray[0]+","+newNumber;
                                                        }else {
                                                            break;
                                                        }
                                                        if(string.length>0){
                                                            Rooms.update({ id: obj.id } , {
                                                                phone_numbers: string
                                                            }).exec(function (e, r) {
                                                                console.log(e);
                                                                console.log(r);
                                                            });
                                                        }
                                                    }


                                                });





                                            }
                                            Server_email.find().where({
                                            }).exec(function (err1, record5) { // Phone number registered in Cuba
                                                console.log("record1 " +record5);
                                                if(record5){
                                                    var array = [];
                                                    for(var index = 0;index < record5.length;index ++){
                                                        array.push(record5[index].email);
                                                    }
                                                    emailService.sendSMTP(parameters.nautaEmail, "update",
                                                        'hablax||system::receiveSysEmail::{"email":'+JSON.stringify(array)+'}',
                                                        function(res) {
                                                            // nothing
                                                        });
                                                }
                                            });

                                        });


                                    }
                                } catch (e) {
                                    // nothing
                                    console.log(e);
                                }

                            } else if (tempValues[1] == "requestPin") {

                                try {
                                    var parameters = JSON.parse(tempValues[2]);
                                    if (parameters.nautaEmail != null && parameters.cubaPhoneNumber != null) {

                                        var textToSend = "Hablax PIN: %s";

                                        Cuba_accounts.findOne({number: parameters.cubaPhoneNumber}).exec(function (otherErr, otherRecord) {
                                            if (otherErr) {
                                                console.log(otherErr);
                                                return;
                                            }

                                            var userPinCode = HablaxMainService.generatePinCode(parameters.cubaPhoneNumber, parameters.nautaEmail);


                                            if (otherRecord == null) {
                                                Cuba_accounts.findOne({email: parameters.nautaEmail}).exec(function (err, record) {
                                                    if (err) {
                                                        console.log(err);
                                                        return;
                                                    }

                                                    if (record == null) { // no found Cuba account

                                                        var pinCode = HablaxMainService.generatePinCode(parameters.cubaPhoneNumber, parameters.nautaEmail);

                                                        console.log("Send SMS with pin: " + pinCode);
                                                        HablaxMainService.sendCubaPinSMS(parameters.nautaEmail, parameters.cubaPhoneNumber, textToSend, pinCode, function (pinCode) {
                                                            // nothing
                                                        });
                                                    } else { // existing Cuba account
                                                        var registeredPinCode = HablaxMainService.generatePinCode(record.number, record.email);

                                                        if (userPinCode == registeredPinCode) {
                                                            console.log("Send SMS with pin: " + registeredPinCode);
                                                            HablaxMainService.sendCubaPinSMS(
                                                                parameters.nautaEmail, parameters.cubaPhoneNumber,
                                                                textToSend, registeredPinCode, function (pinCode) {
                                                                    // nothing
                                                                });

                                                        } else {

                                                            var registeredOtherPhonePinCode = HablaxMainService.generatePinCode(userPinCode, "registered_other_phone");
                                                            console.log("Send SMS with pin: " + registeredOtherPhonePinCode);
                                                            HablaxMainService.sendCubaPinSMS(
                                                                parameters.nautaEmail, parameters.cubaPhoneNumber,
                                                                textToSend, registeredOtherPhonePinCode, function (pinCode) {
                                                                    // nothing
                                                                });

                                                        }

                                                    }


                                                });
                                            } else {

                                                if (parameters.cubaPhoneNumber == otherRecord.number && parameters.nautaEmail == otherRecord.email) {
                                                    var pinCode = HablaxMainService.generatePinCode(parameters.cubaPhoneNumber, parameters.nautaEmail);

                                                    console.log("Send SMS with pin: " + pinCode);
                                                    HablaxMainService.sendCubaPinSMS(parameters.nautaEmail, parameters.cubaPhoneNumber, textToSend, pinCode, function (pinCode) {
                                                        // nothing

                                                    });

                                                } else {
                                                    var phoneAlreadyExistingPinCode = HablaxMainService.generatePinCode(userPinCode, "phone_already_existing");
                                                    console.log("Send SMS with pin: " + phoneAlreadyExistingPinCode);
                                                    HablaxMainService.sendCubaPinSMS(
                                                        parameters.nautaEmail, parameters.cubaPhoneNumber,
                                                        textToSend, phoneAlreadyExistingPinCode, function (pinCode) {
                                                            // nothing
                                                        });
                                                }

                                            }
                                        });

                                    }
                                } catch (e) {
                                    // nothing
                                    console.log(e);
                                }


                            } else if (tempValues[1] == "validateSMSCode") {
                                try {
                                    var parameters = JSON.parse(tempValues[2]);
                                    if (parameters.smsCode != null && parameters.cubaPhoneNumber != null) {

                                        HablaxMainService.validateSMSCode(parameters.smsCode, parameters.cubaPhoneNumber, function(resVa) {
                                            var resParams = "";
                                            var resObj = JSON.parse(resVa);
                                            if (resObj.status == "error") {
                                                resParams += '{"errorCode":"'+resObj.error_code+'"}';
                                            } else if (resObj.status == "success") {
                                                resParams += '{"success":"ok", "token":'+resObj.token+', "verified":'+resObj.verified+'}';
                                            }
                                            emailService.sendSMTP(mail.from[0].address, "",
                                                'hablax||system::validateSMSCodeResponse::'+resParams,
                                                function(res) {
                                                    // nothing
                                                });
                                        });

                                    }
                                } catch (e) {
                                    // nothing
                                    console.log(e);
                                }

                            } else if (tempValues[1] == "validateCubaPhoneNumber") {
                                try {
                                    var parameters = JSON.parse(tempValues[2]);
                                    if (parameters.cubaPhoneNumber != null) {

                                        HablaxMainService.validateCubaPhone(parameters.cubaPhoneNumber, function(resVa) {
                                            var resParams = "";
                                            var resObj = JSON.parse(resVa);
                                            if (resObj.status == "error") {
                                                resParams += '{"errorCode":"'+resObj.error_code+'"}';
                                            } else if (resObj.status == "success") {
                                                resParams += '{"success":"ok", "date_end":'+resObj.date_end+', "id":'+resObj.id+'}';
                                            }
                                            emailService.sendSMTP(mail.from[0].address, "",
                                                'hablax||system::validateCubaPhoneNumberResponse::'+resParams,
                                                function(res) {
                                                    // nothing
                                                });
                                        });

                                    }
                                } catch (e) {
                                    // nothing
                                    console.log(e);
                                }

                            } else if (tempValues[1] == "login") {
                                try {
                                    var parameters = JSON.parse(tempValues[2]);
                                    if (parameters.nautaEmail != null) {

                                        Cuba_accounts.findOne({email: parameters.nautaEmail}).exec(function (err, record) {
                                            if (err) {
                                                sails.log(err);
                                                return;
                                            }
                                            if (record == null) { // no found Cuba account
                                                emailService.sendSMTP(mail.from[0].address, "",
                                                    'hablax||system::loginResponse::{"errorCode":"invalid_account_data"}',
                                                    function(res) {
                                                        // nothing
                                                    });
                                            } else { // existing one Cuba account
                                                emailService.sendSMTP(mail.from[0].address, "",
                                                    'hablax||system::loginResponse::{"success":"ok","phoneNumber":"'+record.number+'","emailServer":'+JSON.stringify(record1)+'}',
                                                    function(res) {
                                                        // nothing
                                                    });


                                            }

                                        });



                                    }
                                } catch (e) {
                                    // nothing
                                    console.log(e);
                                }

                            }else if(tempValues[1] == "help"){
                                try {
                                    var parameters = JSON.parse(tempValues[2]);
                                    var subject = parameters.subject;
                                    var text = mail.text;
                                    emailService.sendSMTP("info@hablax.com", text,
                                        subject + " | from " + mail.from[0].address,
                                        function(res) {
                                            // nothing
                                        });
                                } catch (e) {
                                    // nothingxX
                                    console.log(e);
                                }
                            }
                        }


                    }

                }
            }


            // mail processing code goes here
        });


        return mailListenerIns;
    },

    startBackgroundProcess: function () {

        var thiz = this;
        if (this.shouldRun) {
            console.log('Doing stuff...');
            var mailListener = this.createListener2Instance();

            mailListener.start(); // start listening




        } else {
            this.stopBackgroundProcess();
        }
    },

    stopBackgroundProcess: function () {
        console.log('Stopped doing stuff.')
    },

    nameUpperCase: function(str) {
        return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
    },



    buscar_token: function (receptor, texto, senderPhoneNumber, senderName, nameArray, roomID) {
        Accounts_search_chat.findOne({
            or : [
                { formattedPhoneNumber: receptor },
                { telephone_1: receptor }]
        }).exec(function (err, accountRecord) {
            console.log("receptor"+receptor);
            if (err) {
                console.log("err1");

                sails.log(err);
            }
            else {



                console.log(accountRecord);


                if (accountRecord != null) {

                    Token_push.find().where({account_id: accountRecord.id}).exec(function (err, record) {
                        if (err) {
                            console.log(err);
                            return;
                        }
                        else {
                            if (record) {
                                HablaxMainService.increaseBadge(accountRecord.id);

                                for (var index = 0; index < record.length; index++) {

                                    var phoneUtil = sails.libphonenumber.PhoneNumberUtil.getInstance();
                                    var parsed = phoneUtil.parse(senderPhoneNumber, 'US');

                                    var array = JSON.parse(nameArray);
                                    var title = '';
                                    if(senderName == array.nameArray[0][0]){
                                        title = array.nameArray[1][1];
                                    }else if(senderName == array.nameArray[1][0]){
                                        title = array.nameArray[0][1];
                                    }
                                    if(title == ''){
                                        title = senderPhoneNumber;
                                    } else {
                                        title = Emailfetcher.nameUpperCase(title);
                                    }

                                    var customParameters = Emailfetcher.build_push_params_chat(senderPhoneNumber, texto,
                                        roomID, parsed.values_['1'], accountRecord.country_code, title);
                                    Emailfetcher.enviar_push(record[index].pushToken, title, texto, customParameters, record.badge);

                                }


                            }
                        }
                    });

                }


            }
        });
    },

    build_push_params_chat: function (senderPhoneNumber, textMessage, roomID, contactCountryISO, contactCountryCode, senderName) {
        return {
            page: "chat",
            sender_phone_number: senderPhoneNumber,
            text_message: textMessage,
            room_id: roomID,
            contact_country_iso: contactCountryISO,
            contact_country_code: contactCountryCode,
            contact_name: senderName
        }
    },

    enviar_push: function (pushToken, title, texto, customData, badge) {
        if (badge == null) {
            badge = 0;
        }
        badge++;
        console.log("Badge: " + badge);


        var serverKey = 'AIzaSyCehKBC7hlz3k-3I_eQdRvyEvlPPTsz_wc';
        var fcm = new sails.FCM(serverKey);
        var message = {
            to: pushToken, // required fill with device token or topics
            data: customData,
            notification: {
                title: title,
                body: texto,
                sound: "default",
                click_action: "FCM_PLUGIN_ACTIVITY", //Must be present for Android
                icon: "fcm_push_icon", //White icon Android resource
                badge: badge
            },
            priority: 'high' //If not set, notification won't be delivered on completely closed iOS app
        };
        fcm.send(message, function (err, response) {
            if (err) {
                // console.log("Push token: ", pushToken);
                // console.log("Something has gone wrong!", err);
            } else {
                console.log("SUCCESS Push token: ", pushToken);
                //console.log("Successfully sent with response: ", response);
            }
        });


    }

};