var emailService = require('../services/EmailService.js');
var Emailfetcher = require('../services/Emailfetcher.js');
var hablaxMainService = require('../services/HablaxMainService.js');

var SOCKET_MESSAGE = "ChatMessage";
var SOCKET_SIGNAL = "ChatSignal";


module.exports = {

    sendSocketMessage: function(receiptAccountEmail, eventName, data, successCB, errorCB) {

        Global_sockets.query("SELECT global_sockets.socket_id FROM global_sockets WHERE global_account_email = '"+receiptAccountEmail+"';", [receiptAccountEmail], function (err1, sockets) {

            if (err1) {
                if (errorCB) {
                    errorCB(err1);
                }
                console.log(err1);
                return;
            }

            if (sockets) {
                for (var index = 0; index < sockets.length; index++) {
                    var socket = sockets[index];
                    console.log(socket);
                    console.log("Send a message to socket: " + JSON.stringify(socket));

                    sails.sockets.broadcast(socket.socket_id, eventName, data);
                }

                if (successCB) {
                    successCB();
                }
            }

        });
    },

    globalSendChatMessage: function (senderId, senderEmail, senderName, senderCountryCode, senderCountryIso,
                            receiptPhoneNumber, receiptCountryCode, receiptCountryIso, textMessage, senderPhoneNumber,
                            token, successCB, errorCB) { // receiptPhoneNumber must be international


        console.log("Detecting receiver...");
        console.log("Receipt country ISO: " + receiptCountryIso);
        console.log("Receipt country code: " + receiptCountryCode);
        if (receiptCountryIso == "CU" || receiptCountryIso == "cu") {

            var searchNumber = "011-" + receiptCountryCode + "-" + receiptPhoneNumber.replace('+' + receiptCountryCode, '');

            if (!senderPhoneNumber.includes("+")) {
                senderPhoneNumber = "+" + senderCountryCode + senderPhoneNumber;
            }
            if (!receiptPhoneNumber.includes("+")) {
                receiptPhoneNumber = "+" + receiptCountryCode + receiptPhoneNumber;
            }

            var formattedMessage = textMessage; // Example: Hello there

            console.log("Sending SMS to cuba person ("+ receiptPhoneNumber +")");
            console.log("SMS message: " + formattedMessage);
            hablaxMainService.sendSms(receiptCountryIso, receiptCountryCode, receiptPhoneNumber, formattedMessage,
                token, function (response) {

                    var track_status = 11;
                    var retError = false;

                    console.log("Testestes11: "+response);
                    console.log("Testestes22: "+response.status);

                    if (response.status == 'error') {

                        retError = true;
                        track_status = 13;

                        if (successCB) {
                            successCB({
                                status: response.status,
                                error_code: response.error_code,
                                createdDate: moment().format("YYYY-MM-DDTHH:mm:ss.SSSZ"),
                                message: textMessage
                            });
                        }
                        return;
                    }

                    Cuba_accounts.findOne({number: receiptPhoneNumber}).exec(function (err, record) { // Phone number registered in Cuba
                        console.log("RECEIPT PHONE NUMBER");
                        console.log(receiptPhoneNumber);
                        if (err) {
                            console.log(err);
                            if (errorCB) {
                                errorCB(err);
                            }
                            return;
                        }

                        if (record) {
                            console.log("FOUND CUBA ACCOUNT");
                            console.log(record);
                            var name = [];

                            App_contacts.findOne({
                                contact_phone: searchNumber,
                                account_id: senderId
                            }).exec(function (err1, contact) {
                                console.log(1234431);

                                if (record.device_platform == "iOS") {
                                    formattedMessage += " hablax://m";
                                }

                                var receiptName = null;
                                var sendOutside = null;
                                if (contact) {
                                    receiptName = contact.contact_name;
                                } else {
                                    receiptName = '';
                                }
                                    name.push([senderName, receiptName]);
                                    name.push(["", ""]);



                                name = JSON.stringify({nameArray: name});

                                var sendOutside = 0;

                                ChatService.storeNewMessage(senderEmail, record.email, senderPhoneNumber, receiptPhoneNumber,
                                    senderCountryCode, senderCountryIso, receiptCountryCode, receiptCountryIso, name, name,
                                    textMessage, '', sendOutside, track_status, false, 0, function (room_id, message_id, room) {

                                        if (!retError) {     // SUCCESS

                                            if (successCB) {

                                                successCB({
                                                    status: 11,
                                                    createdDate: moment().format("YYYY-MM-DDTHH:mm:ss.SSSZ"),
                                                    message: textMessage,
                                                    id: message_id,
                                                    roomID: room_id,
                                                    room: JSON.stringify(room)

                                                });
                                            }

                                            console.log("HAVE SEND EMAIL");
                                            emailService.sendSMTP(record.email, textMessage,
                                                'hablax||chat::sendMessage::{"sender":"' + senderPhoneNumber
                                                + '","senderCountryCode":"' + senderCountryCode
                                                + '","senderCountryIso":"' + senderCountryIso
                                                + '","senderName":"' + senderName
                                                + '","message":"' + textMessage
                                                + '","senderMessageID":' + message_id
                                                + '}',
                                                function (res) {
                                                    // nothing
                                                });

                                        }
                                    });


                            });
                        } else {
                            var receiptEmail = "";
                            var receiptEmail = "";
                            var receiptName = "";
                            var sendOutside = 1;
                            var name = [[senderName, ""], ["", ""]];
                            name = JSON.stringify({nameArray: name});

                            ChatService.storeNewMessage(senderEmail, receiptEmail, senderPhoneNumber, receiptPhoneNumber,
                                senderCountryCode, senderCountryIso, receiptCountryCode, receiptCountryIso, name, name,
                                textMessage, '', sendOutside, track_status, false, 0, function (room_id, message_id, room) {
                                    console.log("done 17");


                                    if (!retError) {
                                        if (successCB) {
                                            successCB({
                                                status: 11,
                                                createdDate: moment().format("YYYY-MM-DDTHH:mm:ss.SSSZ"),
                                                message: textMessage,
                                                id: message_id,
                                                roomID: room_id,
                                                room: JSON.stringify(room)

                                            });

                                        }

                                    }
                                });


                        }
                    });

                }, function (error) {

                    console.log("ERROR");
                });


        }
        else {

            var searchNumber = "011-" + receiptCountryCode + "-" + receiptPhoneNumber.replace('+' + receiptCountryCode, '');
            console.log("searchNumberabc: "+receiptCountryCode);
            console.log("searchNumberabc: "+receiptPhoneNumber);
            console.log("searchNumberabc: "+searchNumber);
            if (!senderPhoneNumber.includes("+")) {
                senderPhoneNumber = "+" + senderCountryCode + senderPhoneNumber;
            }
            if (!receiptPhoneNumber.includes("+")) {
                receiptPhoneNumber = "+" + receiptCountryCode + receiptPhoneNumber;
            }



                Accounts_search_chat.findOne({
                    or: [
                        {formattedPhoneNumber: receiptPhoneNumber},
                        {telephone_1: receiptPhoneNumber}]
                }).exec(function (err1, internationalRecord) {

                    if (err1) {
                        console.log("Error in find one account");
                        console.log(err1);
                        return;
                    }



                    var name = [];
                    if (internationalRecord) {
                        console.log("FOUND INTERNATIONAL ACCOUNT");


                        var receiptEmail = internationalRecord.email;
                        App_contacts.findOne({
                            contact_phone: searchNumber,
                            account_id: senderId
                        }).exec(function (err1, contact) {

                            var receiptName = null;
                            if (contact) {
                                receiptName = contact.contact_name;
                            } else {
                                receiptName = '';
                            }


                            var searchNumber1 = "011-" + senderCountryCode + "-" + senderPhoneNumber.replace('+' + senderCountryCode, '');
                            App_contacts.findOne({
                                contact_phone: searchNumber1,
                                account_id: internationalRecord.id
                            }).exec(function (err2, contact1) {


                                var receiptName1 = null;
                                if (contact1) {
                                    receiptName1 = contact1.contact_name;
                                } else {
                                    receiptName1 = '';
                                }

                                name.push([senderName, receiptName]);
                                name.push([internationalRecord.first_name + " " + internationalRecord.last_name, receiptName1]);


                                console.log("name " + name);
                                name = JSON.stringify({nameArray: name});
                                console.log("name " + name);
                                var sendOutside = 0;
                                ChatService.storeNewMessage(senderEmail, receiptEmail, senderPhoneNumber, receiptPhoneNumber,
                                    senderCountryCode, senderCountryIso, receiptCountryCode, receiptCountryIso,
                                    name, name, textMessage, '', sendOutside, 11, false, 0, function (room_id, message_id, room) {

                                        Emailfetcher.buscar_token(receiptPhoneNumber, textMessage, senderPhoneNumber, senderName, name, room_id);

                                        console.log("receiptEmail " + receiptEmail);


                                        var socketData = {
                                             page: "chat",
                                             senderPhoneNumber: senderPhoneNumber,
                                             textMessage: textMessage,
                                             roomID: room_id,
                                             contact_country_iso: senderCountryIso,
                                             contact_phone: senderPhoneNumber,
                                             messageId: message_id,
                                             state: 11,
                                             senderEmail: senderEmail
                                         }
                                        ChatService.sendSocketMessage(receiptEmail, SOCKET_MESSAGE, socketData, function() {
                                                if (successCB) {

                                                    successCB({
                                                        status: response.status,
                                                        createdDate: moment().format("YYYY-MM-DDTHH:mm:ss.SSSZ"),
                                                        message: textMessage,
                                                        id: message_id,
                                                        roomID: room_id,
                                                        room: JSON.stringify(room)

                                                    });
                                                }
                                        });

                                    });
                            });

                        });



                    } else {

                        console.log("No international account existing!");
                        console.log("Sending SMS to international person ("+ receiptPhoneNumber +")");
                        console.log("SMS message: " + textMessage);
                        hablaxMainService.sendSms(receiptCountryIso, receiptCountryCode, receiptPhoneNumber, textMessage, token, function (response) {
                            console.log("response "+JSON.stringify(response));
                            var retError = false;
                            var track_status = 11;

                            if (response.status == 'error') {
                                retError = true;
                                track_status = 13;
                                var obj = {
                                    status: response.status,
                                    error_code: response.error_code,
                                    createdDate: moment().format("YYYY-MM-DDTHH:mm:ss.SSSZ"),
                                    message: textMessage
                                };
                            }


                            App_contacts.findOne({
                                                    contact_phone: searchNumber,
                                                    account_id: senderId
                                                }).exec(function (err1, contact) {


                                var receiptName = null;
                                if (contact) {
                                    receiptName = contact.contact_name;
                                } else {
                                    receiptName = '';
                                }

                                var receiptEmail = "";
                                var receiptName = "";
                                var sendOutside = 1;

                                console.log('Namesender: '+senderName);
                                console.log('Namereceiver: '+receiptName);

                                name.push([senderName, receiptName]);
                                name.push(["", ""]);
                                console.log('namepush: ' +name);
                                name = JSON.stringify({nameArray: name});
                                ChatService.storeNewMessage(senderEmail, receiptEmail, senderPhoneNumber, receiptPhoneNumber,
                                    senderCountryCode, senderCountryIso, receiptCountryCode, receiptCountryIso,
                                    name, name, textMessage, '', sendOutside, track_status, false, 0, function (room_id, message_id, room) {
                                        if (!retError) {
                                            if (successCB) {
                                                successCB({
                                                    status: 11,
                                                    createdDate: moment().format("YYYY-MM-DDTHH:mm:ss.SSSZ"),
                                                    message: textMessage,
                                                    id: message_id,
                                                    roomID: room_id,
                                                    room: JSON.stringify(room)

                                                });
                                            }
                                        }else if(retError){
                                            if(errorCB){
                                                errorCB("");
                                            }
                                        }
                                    });

                            });


                        }, function (error) {
                            if (errorCB) {
                                errorCB(error);
                            }
                            console.log("ERROR");
                        });
                    }

                });



        }


    },

    storeNewMessage: function (email_user_from, email_user_to, fromPhoneNumber, toPhoneNumber, country_code_from,
                               country_iso_from, country_code_to, country_iso_to, name_to, name_from, text_message,
                               admin_notes, send_outside, track_status, isPendingText, senderMessageID, success, error) {
        console.log("Start to store new message...");
        if (send_outside == null) {
            send_outside = 0;
        }
        var pending = 0;
        if (isPendingText != null && isPendingText == true) {
            if (country_iso_to != "CU" || country_iso_to != "cu") {
                pending = 1; // pending message
            }
        }
        var name = JSON.parse(name_to);
        name = JSON.stringify(name.nameArray);

        console.log("Finding room to save messages...");
        Rooms.findOne().where({
            or: [
                {phone_numbers: {like: "%" + fromPhoneNumber + "%" + toPhoneNumber + "%"}},
                {phone_numbers: {like: "%" + toPhoneNumber + "%" + fromPhoneNumber + "%"}}
            ]

        }).exec(function (err1, record) {

            if (err1) {
                console.log(err1);
                if (error) {
                    error(err1);
                }
                return;
            }

            if (record) {
                console.log("A room found to put new message into!");
                console.log(record);
                var now = moment().format("YYYY-MM-DDTHH:mm:ss.SSSZ");
                var phoneNumbersArray = record.phone_numbers.split(",");

                if(phoneNumbersArray.length == 2){
                    if(phoneNumbersArray[0] != fromPhoneNumber && phoneNumbersArray[1] != toPhoneNumber){
                        var temp = JSON.parse(name);
                        var tempName = [temp[1],temp[0]];
                        name = JSON.stringify(tempName);
                    }
                }

                console.log("Update room: time_update, recent_message, member_names...");

                Rooms.update({id: record.id}, {
                    time_update: now,
                    recent_message: text_message,
                    pending: pending,
                    member_names: name,
                    visible_with_user: '1,1'
                }).exec(function (e, r) {
                    if (e) {
                        console.log(e);
                        return;
                    }

                    if (r) {
                        console.log(r);
                    }

                });


                console.log("Storing new message...");
                Messages.create({
                    id: 0,
                    email_user_from: email_user_from,
                    email_user_to: email_user_to,
                    text_message: text_message,
                    admin_notes: admin_notes,
                    room_id: record.id,
                    track_status: track_status,
                    createdAt: now,
                    updatedAt: now,
                    visible_with_user: '1,1',
                    order_in_room: senderMessageID

                }).exec(function (e1, r1) {
                    if (e1) {
                        console.log(e1);
                        if (error) {
                            error(e1);
                        }
                        return;
                    }

                    if (r1) {
                        console.log("DONE store new message!");
                        console.log(r1);
                        if (success) {
                            success(record.id, r1.id, record);
                        }
                    }

                });

            } else {
                console.log("No room exists, creating new room and new message");
                Rooms.create({
                    id: 0,
                    room_members: email_user_from + "," + email_user_to,
                    phone_numbers: fromPhoneNumber + "," + toPhoneNumber,
                    member_names: name,
                    recent_message: text_message,
                    pending: pending,
                    country_code: country_code_from + "," + country_code_to,
                    country_code_iso: country_iso_from + "," + country_iso_to,
                    send_outside: send_outside,
                    createdAt: moment().format("YYYY-MM-DDTHH:mm:ss.SSSZ"),
                    time_update: moment().format("YYYY-MM-DDTHH:mm:ss.SSSZ"),
                    visible_with_user: '1,1'

                }).exec(function (e, room) {

                    Messages.create({
                        id: 0,
                        email_user_from: email_user_from,
                        email_user_to: email_user_to,
                        text_message: text_message,
                        admin_notes: admin_notes,
                        track_status: track_status,
                        room_id: room.id,
                        createdAt: moment().format("YYYY-MM-DDTHH:mm:ss.SSSZ"),
                        updatedAt: moment().format("YYYY-MM-DDTHH:mm:ss.SSSZ"),
                        visible_with_user: '1,1',
                        order_in_room: senderMessageID

                    }).exec(function (e, r) {

                        if (e) {
                            console.log(e);
                            if (error) {
                                error(e);
                            }
                            return;
                        }

                        if (r) {
                            console.log("DONE store new message!");
                            if (success) {
                                success(room.id, r.id, room);
                            }
                        }


                    });
                });
                //Accounts_contacts.findOne().where();


            }

        });


    },
    globalFetchAllChatRoom: function (myEmail, successCB, errorCB) {
        var sortString = 'time_update DESC';
        var myQuery = Rooms.find().where({
            room_members: {like: '%' + myEmail + '%'}
        });

        myQuery.sort(sortString);
        myQuery.exec(function (err1, records) { // Phone number registered in Cuba
            if (err1) {
                console.log(err1);
                if (errorCB) {
                    errorCB(err1);
                }

                return;
            }
            console.log(records);
            if (records) {
                if (successCB) {
                    successCB(records);
                }
            } else {
                if (successCB) {
                    successCB([]);
                }
            }
        });
    },
    globalFetchUnseenChatRoom: function (from, email_from, successCB, errorCB) {

        console.log("Fetch unseen messages");
        var from = moment(from).format("YYYY-MM-DDTHH:mm:ss.SSSZ");

        console.log("Email: " + email_from);
        console.log("FROM date: " + from);

        Messages.query("SELECT messages.* FROM messages "
                            +"WHERE email_user_to = '"+email_from+"' "
                            +"AND strcmp(createdAt, '"+from+"') = 1;", [email_from, from], function (err1, record) {

            if (err1) {
                console.log(err1);
                if (errorCB) {
                    errorCB(err1);
                }
                return;
            }
            if (record) {
                console.log(record);
                if (successCB) {
                    successCB(record);
                }
            }
        });


    },

    globalDelRoomById: function (roomId,phoneNumberDeleted, successCB, errorCB) {




        Rooms.findOne().where({id: roomId}).exec(function (err1, record) {

            console.log("record "+JSON.stringify(record));
            if (record) {
                var myEmai = null;
                if(record.visible_with_user == '1,1') {
                    var text = null;
                    var emailsArray = record.room_members.split(",");
                    var phoneArray = record.phone_numbers.split(",");
                    if (emailsArray.length == 2 && phoneArray.length == 2) {
                        if (phoneArray[0] == phoneNumberDeleted) {
                            myEmai = emailsArray[0];
                            text = "0,1";
                        } else if (phoneArray[1] == phoneNumberDeleted) {
                            myEmai = emailsArray[1];
                            text = "1,0";
                        }
                        if (myEmai != null && text != null) {
                            console.log("text "+text);
                            console.log("roomId "+roomId);

                            Rooms.update({id: roomId},{visible_with_user: text}).exec(function afterwards(err, updated){
                                if (err) {
                                    if (errorCB) {
                                        errorCB("");
                                    }
                                }
                                if(updated) {

                                    Messages.update({room_id: roomId,email_user_from: myEmai},{visible_with_user: "0,1"}).exec(function afterwards(err2, updated1){
                                        if (err2) {
                                            if (errorCB) {
                                                errorCB("");
                                            }
                                        }
                                        if(updated1) {
                                            Messages.update({room_id: roomId,email_user_to: myEmai},{visible_with_user: "1,0"}).exec(function afterwards(err3, updated3){
                                                if (err3) {
                                                    if (errorCB) {
                                                        errorCB("");
                                                    }
                                                }
                                                if(updated3) {
                                                    if (successCB) {
                                                        successCB("");
                                                    }
                                                }
                                            });
                                        }
                                    });
                                }
                            });

                        } else {
                                if (errorCB) {
                                    errorCB("");
                                }

                        }
                    } else {
                            if (errorCB) {
                                errorCB("");
                            }
                    }
                }else {
                    Rooms.destroy({id: roomId}).exec(function (err) {
                        if (err) {
                            console.log(err);
                            if (errorCB) {
                                errorCB(err);
                            }
                            return;
                        }

                        Messages.destroy({room_id: roomId}).exec(function (err1) {
                            if (err1) {
                                console.log(err1);
                                if (errorCB) {
                                    errorCB(err1);
                                }
                                return;
                            }

                            if (successCB) {
                                console.log("Delete Success");
                                successCB("OK");
                            }
                        });

                    });

                }
            }
        });
    },

    stopSocketChatConnection: function (token, successCB, errorCB) {

        console.log("token " + token);
        Global_sockets.destroy({global_account_token: token}).exec(function (err) {
            if (err) {
                console.log(err);
                if (errorCB) {
                    errorCB(err);
                }
                return;
            }
            if (successCB) {
                console.log("Delete Success");
                successCB("OK");
            }
        });


    },
    globalFetchAllMessages: function (roomId, successCB, errorCB) {

        Rooms.findOne().where({id: roomId}).exec(function (err1, record) {
            if (err1) {
                if (errorCB) {
                    errorCB(err1);
                }
                return;
            }
            if (record) {
                Messages.find().where({
                    room_id: roomId
                }).sort("").exec(function (err1, record1) { // Phone number registered in Cuba
                    if (err1) {
                        console.log(err1);
                        if (errorCB) {
                            errorCB(err1);
                        }
                        return;
                    }
                    if (record1) {

                        if (successCB) {
                            var data = {messageList: record1, room: record};
                            successCB(data);
                        }
                    } else {

                        if (successCB) {
                            successCB([]);
                        }
                    }
                });
            } else {
                if (successCB) {
                    successCB([]);
                }
            }

        });

    },fetchAllMessagesAndRoomByRoomID: function (room_id, successCB, errorCB) {

        Rooms.findOne().where({ id: room_id }).exec(function (err1, record) {
            if (err1) {
                if (errorCB) {
                    errorCB(err1);
                }
                return;
            }
            if (record) {
                Messages.find().where({
                    room_id: record.id
                }).exec(function (err1, record1) { // Phone number registered in Cuba

                    if (err1) {
                        console.log(err1);
                        if (errorCB) {
                            errorCB(err1);
                        }
                        return;
                    }
                    if (record1) {

                        if (successCB) {
                            var data = {messageList: record1, room: record};
                            successCB(data);
                        }
                    } else {
                        if (successCB) {
                            successCB([]);
                        }
                    }
                });
            } else {
                if (successCB) {
                    successCB([]);
                }
            }

        });

    },fetchAllMessagesByPhoneNumber: function (phone_number_from, phone_number_to, successCB, errorCB) {


        Rooms.findOne().where({
            or: [
                {phone_numbers: {like: "%" + phone_number_from + "%" + phone_number_to + "%"}},
                {phone_numbers: {like: "%" + phone_number_to + "%" + phone_number_from + "%"}}
            ]

        }).exec(function (err1, record) {
            if (err1) {
                if (errorCB) {
                    errorCB(err1);
                }
                return;
            }
            if (record) {
                Messages.find().where({
                    room_id: record.id
                }).exec(function (err1, record1) { // Phone number registered in Cuba

                    if (err1) {
                        console.log(err1);
                        if (errorCB) {
                            errorCB(err1);
                        }
                        return;
                    }
                    if (record1) {

                        if (successCB) {
                            var data = {messageList: record1, room: record};
                            successCB(data);
                        }
                    } else {
                        if (successCB) {
                            successCB([]);
                        }
                    }
                });
            } else {
                if (successCB) {
                    successCB([]);
                }
            }

        });


    },
    updateMessageStatus: function (roomId, email_from, email_to, iso_from,
                                phone_number_from, phone_number_to, successCB, errorCB) {
        console.log("roomId " + roomId);
        console.log("email_from " + email_from);
        console.log("email_to " + email_to);
        console.log("iso_from " + iso_from);
        console.log("phone_number_from " + phone_number_from);
        console.log("phone_number_to " + phone_number_to);
        Messages.update({
            email_user_from: email_from,
            email_user_to: email_to,
            room_id: roomId,
            track_status: 11
        }, {track_status: 12}).exec(function (e, r) {
            if (e) {
                if (errorCB) {
                    console.log(e);
                    errorCB(e);
                }
                return;
            }
            if (successCB) {
                console.log(r);
                if (r) {
                    if (iso_from == "CU" || iso_from == "cu") {
                        var max_cuba_message_id = 0;
                        for (var ind = 0; ind < r.length; ind++) {
                            var item = r[ind];
                            if (item.order_in_room > max_cuba_message_id) {
                                max_cuba_message_id = item.order_in_room;
                            }

                        }

                        console.log("email_from "+email_from);
                        emailService.sendSMTP(email_from, "update",
                            'hablax||chat::trackMessageStatus::{"phone_number_from":"' + phone_number_from + '","phone_number_to":"' + phone_number_to + '","cuba_message_id":' + max_cuba_message_id + '}',
                            function (res) {
                                // nothing
                            });
                    } else {
                        console.log("international socket");
                        console.log("\"Socket_\"+email_from " + "Socket_" + email_from);


                        var socketData = {
                            email_from: email_from,
                            room_id: roomId,
                            messages: r
                        }
                        ChatService.sendSocketMessage(email_from, SOCKET_SIGNAL, socketData, function() {
                            // nothing
                        });

                    }

                    successCB(r);
                }
            }
        });
    },
    unlockChatMessage: function (accountId, messageRowId, room_id, token, successCB, errorCB) {



        Messages.findOne({id: messageRowId}).exec(function (errM, message) {

            if (errM) {
                console.log(errM);
                return;
            }

            if (message) {
                Cuba_accounts.findOne({email: message.email_user_from}).exec(function (errC, cubaRecord) {
                    if (errC) {
                        console.log(errC);
                        return;
                    }

                    if (cubaRecord) {
                        hablaxMainService.charge_for_message_reception(accountId,
                                                        cubaRecord.iso_code, "53",
                                                        cubaRecord.number.replace('+53', ''),
                                                        message.text_message,
                                                        function (response) {

                            console.log("response" + JSON.stringify(response));
                            console.log("response.status " + JSON.stringify(response.status));

                            if (response.status != null && response.status == 'error') {

                                if (errorCB) {
                                    errorCB(response.status);
                                    return;
                                }
                                if (successCB) {
                                    successCB(response.status);
                                    return;
                                }
                            }

                            console.log("messageRowId : " + messageRowId);
                            console.log("room_id : " + room_id);

                            Messages.update({
                                id: messageRowId,
                                room_id: room_id,
                                track_status: 14
                            }, {track_status: 11}).exec(function (e, r) {
                                if (e) {
                                    console.log(e);
                                    if (errorCB) {
                                        errorCB(e);
                                    }
                                    return;
                                }
                                if (r) {
                                    if (successCB) {
                                        console.log("room_id " + room_id);
                                        Rooms.update({
                                            id: room_id,
                                            time_update: moment().format("YYYY-MM-DDTHH:mm:ss.SSSZ"),
                                            pending: 1
                                        }, {pending: 0}).exec(function (e1, r1) {
                                            if (e) {
                                                if (errorCB) {
                                                    console.log(e1);
                                                    errorCB(e1);
                                                    return;
                                                }
                                                return;
                                            }
                                            successCB(r);

                                        });
                                    }
                                }
                            });


                        }, function (error) {

                            console.log("ERROR");
                        });
                    }
                });
            }


        });






    },
    resendChatMessage: function (messageRowId, room_id, textMessage, senderPhoneNumber,
                                receiptPhoneNumber, receiptCountryCode, receiptCountryIso,
                                token, senderId, senderName, senderCountryCode, senderCountryIso,
                                senderEmail, successCB, errorCB) {
        if (receiptCountryIso == "CU" || receiptCountryIso == "cu") {
            if (!senderPhoneNumber.includes("+")) {
                senderPhoneNumber = "+" + senderCountryCode + senderPhoneNumber;
            }
            if (!receiptPhoneNumber.includes("+")) {
                receiptPhoneNumber = "+" + receiptCountryCode + receiptPhoneNumber;
            }
            console.log("text " + textMessage);
            var formattedMessage = textMessage; // Example: Hello there
            hablaxMainService.sendSms(receiptCountryIso, receiptCountryCode, receiptPhoneNumber, formattedMessage,
                token, function (response) {
                    console.log(response);
                    console.log(response.status);
                    var track_status = 11;
                    var retError = false;
                    if (response.status == 'error') {
                        console.log(response.error_code);
                        retError = true;
                        track_status = 13;
                        if (successCB) {
                            successCB({
                                status: response.status,
                                error_code: response.error_code,
                                createdDate: moment().format("YYYY-MM-DDTHH:mm:ss.SSSZ"),
                                message: textMessage
                            });
                        }
                    }

                    if (!retError) { // SUCCESS

                        if (successCB) {
                            successCB({
                                status: response.status,
                                createdDate: moment().format("YYYY-MM-DDTHH:mm:ss.SSSZ"),
                                message: textMessage
                            });
                        }

                        Cuba_accounts.findOne({number: receiptPhoneNumber}).exec(function (err, record) { // Phone number registered in Cuba
                            if (record) {
                                    console.log("HAVE SEND EMAIL");
                                    emailService.sendSMTP(record.email, textMessage,
                                        'hablax||chat::sendMessage::{"sender":"' + senderPhoneNumber
                                        + '","senderCountryCode":"' + senderCountryCode
                                        + '","senderCountryIso":"' + senderCountryIso
                                        + '","senderName":"' + senderName
                                        + '","message":"' + textMessage + '"}',
                                        function (res) {
                                            // nothing
                                        });
                            }
                        });


                    }


                }, function (error) {

                    console.log("ERROR");
                });


        }
        else {

            var searchNumber = "011-" + receiptCountryCode + "-" + receiptPhoneNumber.replace('+' + receiptCountryCode, '');
            if (!senderPhoneNumber.includes("+")) {
                senderPhoneNumber = "+" + senderCountryCode + senderPhoneNumber;
            }
            if (!receiptPhoneNumber.includes("+")) {
                receiptPhoneNumber = "+" + receiptCountryCode + receiptPhoneNumber;
            }

            hablaxMainService.sendSms(receiptCountryIso, receiptCountryCode, receiptPhoneNumber, textMessage, token, function (response) {
                console.log(response);

                var retError = false;
                var track_status = 11;

                if (response.status == 'error') {
                    console.log(response.error_code);

                    retError = true;
                    track_status = 13;

                    if (successCB) {
                        successCB({
                            status: response.status,
                            error_code: response.error_code,
                            createdDate: moment().format("YYYY-MM-DDTHH:mm:ss.SSSZ"),
                            message: textMessage
                        });
                    }
                }

                Accounts_search_chat.findOne({
                    or: [
                        {formattedPhoneNumber: receiptPhoneNumber},
                        {telephone_1: receiptPhoneNumber}]
                }).exec(function (err1, internationalRecord) {

                    if (err1) {
                        console.log("Error in find one account");
                        console.log(err1);
                        return;
                    }
                    console.log("FOUND INTERNATIONAL ACCOUNT");


                    var name = [];
                    if (internationalRecord) {
                        var receiptEmail = internationalRecord.email;
                        App_contacts.findOne({
                            contact_phone: searchNumber,
                            account_id: senderId
                        }).exec(function (err1, contact) {

                            var receiptName = null;
                            if (contact) {
                                receiptName = contact.contact_name;
                            } else {
                                receiptName = '';
                            }

                            name.push([senderName, receiptName]);


                            var searchNumber1 = "011-" + senderCountryCode + "-" + senderPhoneNumber.replace('+' + senderCountryCode, '');
                            App_contacts.findOne({
                                contact_phone: searchNumber1,
                                account_id: internationalRecord.id
                            }).exec(function (err2, contact1) {


                                var receiptName1 = null;
                                if (contact1) {
                                    receiptName1 = contact1.contact_name;
                                } else {
                                    receiptName1 = '';
                                }

                                name.push([internationalRecord.first_name + " " + internationalRecord.last_name, receiptName1]);
                                console.log("name " + name);
                                name = JSON.stringify({nameArray: name});
                                console.log("name " + name);
                                var sendOutside = 0;

                                Messages.update({
                                    id: messageRowId,
                                    room_id: room_id,
                                    or: [
                                        {track_status: 13},
                                        {track_status: 14}]
                                }, {track_status: 11}).exec(function (e, r) {
                                    if (e) {
                                        if (errorCB) {
                                            console.log(e);
                                            errorCB(e);
                                        }
                                        return;
                                    }
                                    if (successCB) {
                                        if (r) {
                                            Emailfetcher.buscar_token(receiptPhoneNumber, textMessage, senderPhoneNumber, senderName, name, room_id);

                                            var socketData = {
                                                page: "chat",
                                                senderPhoneNumber: senderPhoneNumber,
                                                textMessage: textMessage,
                                                roomID: room_id,
                                                contact_country_iso: senderCountryIso,
                                                contact_phone: senderPhoneNumber,
                                                messageId: message_id,
                                                senderEmail: senderEmail
                                            }
                                            ChatService.sendSocketMessage(receiptEmail, SOCKET_MESSAGE, socketData, function() {
                                                if (!retError) {
                                                    if (successCB) {

                                                        successCB({
                                                            createdDate: moment().format("YYYY-MM-DDTHH:mm:ss.SSSZ"),
                                                            message: textMessage,
                                                            id: messageRowId
                                                        });
                                                    }
                                                }
                                            });



                                        }
                                    }
                                });


                            });

                        });

                    } else {
                        if (successCB) {
                            successCB({
                                createdDate: moment().format("YYYY-MM-DDTHH:mm:ss.SSSZ"),
                                message: textMessage
                            });
                        }
                    }

                });
            }, function (error) {
                if (errorCB) {
                    errorCB(error);
                }
                console.log("ERROR");
            });


        }


    }, handle_message_from_cuba: function (parameters, iso, code) {

        var account = null;
        var senderEmail = parameters.senderEmail;
        var receiptPhoneNumber = parameters.receiptPhoneNumber;
        var senderPhoneNumber = parameters.senderPhoneNumber;
        var senderCountryCode = parameters.senderCountryCode;
        var senderCountryIso = parameters.senderCountryIso;

        var receiptCountryCode = parameters.receiptCountryCode;
        var receiptCountryIso = parameters.receiptCountryIso;

        var textMessage = parameters.textMessage;

        var cubaSenderMessageID = parameters.cubaMessageID;


        if ((receiptCountryIso == null || receiptCountryIso == "null") && (receiptCountryCode == null || receiptCountryCode == "null")) {
            receiptCountryCode = code;
            receiptCountryIso = iso;
        }

        if (receiptCountryIso == "CU" || receiptCountryIso == "cu") {

            console.log("Sending EMAIL message to CUBA person ("+ receiptPhoneNumber +" - "+ receiptEmail +")");
            Cuba_accounts.findOne({number: receiptPhoneNumber}).exec(function (err, cubaRecord) {

                if (err) {
                    console.log(err);
                    return;
                }
                if (!senderPhoneNumber.includes("+")) {
                    senderPhoneNumber = "+" + senderCountryCode + senderPhoneNumber;
                }
                if (!receiptPhoneNumber.includes("+")) {
                    receiptPhoneNumber = "+" + receiptCountryCode + receiptPhoneNumber;
                }
                var name = [];
                name.push(["", ""]);
                name.push(["", ""]);
                name = JSON.stringify({nameArray: name});
                if (cubaRecord != null && cubaRecord != undefined) {
                    account = cubaRecord;
                    // console.log(cubaRecord)

                    if (account) {


                        var receiptEmail = account.email;

                        var sendOutside = 0;


                            emailService.sendSMTP(receiptEmail, textMessage,
                                'hablax||chat::sendMessage::{'
                                +'"sender":"' + senderPhoneNumber
                                + '","senderName":""'
                                + ',senderCountryCode":"' + senderCountryCode
                                + '","senderCountryIso":"' + senderCountryIso
                                + '","message":"' + textMessage
                                + '","senderMessageID":' + cubaSenderMessageID
                                + '}',
                                function (res) {
                                    console.log("Email sent!");
                                });

                        ChatService.storeNewMessage(senderEmail, receiptEmail,
                            senderPhoneNumber, receiptPhoneNumber, senderCountryCode,
                            senderCountryIso, receiptCountryCode, receiptCountryIso,
                            name, name, textMessage, '', sendOutside, 11, false, cubaSenderMessageID, function (room_id) {

                                // nothing
                            });

                    }
                } else {
                    var sendOutside = 1;
                    var receiptEmail = "";
                    ChatService.storeNewMessage(senderEmail, receiptEmail, senderPhoneNumber,
                        receiptPhoneNumber, senderCountryCode,
                        senderCountryIso, receiptCountryCode,
                        receiptCountryIso, name, name, textMessage,
                        '', sendOutside, 11, false, cubaSenderMessageID, function (room_id) {

                            // nothing
                        });
                }
            });
        } else {


            console.log("Sending SOCKET message to international person ("+ receiptPhoneNumber +")");
            Accounts_search_chat.findOne({
                or: [
                    {formattedPhoneNumber: receiptPhoneNumber},
                    {telephone_1: receiptPhoneNumber}]
            }).exec(function (err1, record) {

                if (err1) {
                    console.log(err1);
                    return;
                }
                account = record;
                // console.log(record);
                var oneCent = 0.01;
                var state = 11;
                var isPendingText = false;


                if (record) {
                    if (record.balance - oneCent < 0) {
                        state = 14;
                        isPendingText = true;
                    }
                    var sendOutside = 0;
                    var searchNumber = "011-" + senderCountryCode + "-" + senderPhoneNumber.replace('+' + senderCountryCode, '');
                    App_contacts.findOne({
                        contact_phone: searchNumber,
                        account_id: record.id
                    }).exec(function (err1, contact) {

                        if (err1) {
                            console.log(err1);
                            return;
                        }

                        var receiptName = null;

                        var name = new Array();
                        name.push(["", ""]);




                        hablaxMainService.charge_for_message_reception(record.id, senderCountryIso,
                                                        senderCountryCode, senderPhoneNumber.replace('+' + senderCountryCode, ''),
                                                        textMessage, function (response) {

                            if (contact) {
                                receiptName = contact.contact_name;
                                console.log("Found receipt name: " + receiptName);

                                var senderName = record.first_name + " " + record.last_name;
                                if (!senderPhoneNumber.includes("+")) {
                                    senderPhoneNumber = "+" + senderCountryCode + senderPhoneNumber;
                                }
                                if (!receiptPhoneNumber.includes("+")) {
                                    receiptPhoneNumber = "+" + receiptCountryCode + receiptPhoneNumber;
                                }
                                var receiptEmail = record.email;

                                name.push([senderName, receiptName]);
                                name = JSON.stringify({nameArray: name});

                                ChatService.storeNewMessage(senderEmail, receiptEmail,
                                    senderPhoneNumber, receiptPhoneNumber,
                                    senderCountryCode,senderCountryIso,
                                    receiptCountryCode,receiptCountryIso,
                                    name, name, textMessage, '',
                                    sendOutside, state, isPendingText, cubaSenderMessageID, function (room_id, message_id, room) {

                                        Token_push.find().where({account_id: record.id}).exec(function (err, tokenPushRecord) {
                                            if (err) {
                                                console.log(err);
                                                return;
                                            }


                                                if (tokenPushRecord) {

                                                    HablaxMainService.increaseBadge(record.id);

                                                    for (var index = 0; index < tokenPushRecord.length; index++) {

                                                        if(state == 14){
                                                            textMessage = "Pending";
                                                        }
                                                        var customParameters = Emailfetcher.build_push_params_chat(senderPhoneNumber, textMessage, room_id, senderCountryIso, senderCountryCode, senderName);
                                                        Emailfetcher.enviar_push(tokenPushRecord[index].pushToken,
                                                            Emailfetcher.nameUpperCase(receiptName), textMessage, customParameters, record.badge);
                                                    }


                                                }

                                        });

                                        var socketData = {
                                            page: "chat",
                                            senderPhoneNumber: senderPhoneNumber,
                                            textMessage: textMessage,
                                            roomID: room_id,
                                            contact_country_iso: senderCountryIso,
                                            contact_phone: senderPhoneNumber,
                                            messageId: message_id,
                                            state: state,
                                            senderEmail: senderEmail
                                        }

                                        ChatService.sendSocketMessage(receiptEmail, SOCKET_MESSAGE, socketData, function() {
                                            // nothing
                                        });


                                    });

                            } else {
                                receiptName = '';

                                console.log("NOT found receipt name: " + receiptName);

                                var senderName = record.first_name + " " + record.last_name;
                                var receiptEmail = record.email;
                                name.push([senderName, receiptName]);
                                name = JSON.stringify({nameArray: name});

                                if (!receiptPhoneNumber.includes("+")) {
                                    receiptPhoneNumber = "+" + receiptCountryCode + receiptPhoneNumber;
                                }
                                ChatService.storeNewMessage(senderEmail, receiptEmail,
                                    senderPhoneNumber, receiptPhoneNumber,
                                    senderCountryCode, senderCountryIso,
                                    receiptCountryCode, receiptCountryIso,
                                    name, name, textMessage, '', sendOutside,
                                    state, isPendingText, cubaSenderMessageID, function (room_id, message_id, room) {

                                        Token_push.find().where({account_id: record.id}).exec(function (err, tokenPushRecord) {
                                            if (err) {
                                                console.log(err);
                                                return;
                                            }
                                            else {

                                                console.log(tokenPushRecord);

                                                if (tokenPushRecord) {

                                                    HablaxMainService.increaseBadge(record.id);

                                                    for (var index = 0; index < tokenPushRecord.length; index++) {

                                                        if(state == 14){
                                                            textMessage = "Pending";
                                                        }

                                                        var customParameters = Emailfetcher.build_push_params_chat(senderPhoneNumber, textMessage, room_id, senderCountryIso, senderCountryCode, senderName);
                                                        Emailfetcher.enviar_push(tokenPushRecord[index].pushToken,
                                                            senderPhoneNumber, textMessage, customParameters, record.badge);
                                                    }


                                                }
                                            }
                                        });


                                        var socketData = {
                                            page: "chat",
                                            senderPhoneNumber: senderPhoneNumber,
                                            textMessage: textMessage,
                                            roomID: room_id,
                                            contact_country_iso: senderCountryIso,
                                            contact_phone: senderPhoneNumber,
                                            messageId: message_id,
                                            state: state,
                                            senderEmail: senderEmail
                                        }

                                        ChatService.sendSocketMessage(receiptEmail, SOCKET_MESSAGE, socketData, function() {
                                            // nothing
                                        });


                                    });
                            }
                        });

                    });

                } else {
                    var receiptEmail = "";
                    var senderName = "";
                    var receiptName = "";
                    var sendOutside = 1;
                    if (!receiptPhoneNumber.includes("+")) {
                        receiptPhoneNumber = "+" + receiptCountryCode + receiptPhoneNumber;
                    }
                    var name = [["", ""], ["", ""]];
                    name = JSON.stringify({nameArray: name});


                    ChatService.storeNewMessage(senderEmail, receiptEmail, senderPhoneNumber,
                        receiptPhoneNumber, senderCountryCode, senderCountryIso,
                        receiptCountryCode, receiptCountryIso, name, name,
                        textMessage, '', sendOutside, 11, false, cubaSenderMessageID, function (room_id) {
                            console.log("done 1");
                        });
                }
            });
        }
    }
};