module.exports = {

    mainURL: "www.hablax.com",

    sendSMTP: function (emailTo, textMessage, subject, successCB, errorCB) {
        mailConfig = {
            host: 'smtp.gmail.com',
            port: 465,
            secure: true,
            auth: {
                user: 'hablax.demo@gmail.com',
                pass: 'hablax123456'
            }
        };


        var smtpTransport = sails.nodemailer.createTransport(mailConfig);


        // setup email data with unicode symbols
        let mailOptions = {
            from: 'hablax.demo@gmail.com', // sender address
            to: emailTo, // list of receivers
            subject: subject, // Subject line
            text: textMessage, // plain text body
            html: '' // html body
        };

        // send mail with defined transport object
        smtpTransport.sendMail(mailOptions, function(error, response){

            if(error){
                if (errorCB) {
                    errorCB(error);
                }
                console.log(error);
                return;
            }

            smtpTransport.close(); // shut down the connection pool, no more messages
            if (successCB) {
                successCB(response);
            }



        });
    },sendSMTPCustom: function (url, successCB, errorCB) {
        mailConfig = {
            host: 'smtp.gmail.com',
            port: 465,
            secure: true,
            auth: {
                user: 'hablax.demo@gmail.com',
                pass: 'hablax123456'
            }
        };


        var smtpTransport = sails.nodemailer.createTransport(mailConfig);


        // setup email data with unicode symbols
        let mailOptions = {
            from: 'hablax.demo@gmail.com', // sender address
            to: "dnduc456@gmail.com", // list of receivers
            subject: "asd", // Subject line
            text: "dsa", // plain text body
            html: '',
            attachments: [{
                filename: 'image.png',
                path: url,
                cid: 'unique@kreata.ee' //same cid value as in the html img src
            }] // html body
        };

        // send mail with defined transport object
        smtpTransport.sendMail(mailOptions, function(error, response){

            if(error){
                if (errorCB) {
                    errorCB(error);
                }
                console.log(error);
                return;
            }

            smtpTransport.close(); // shut down the connection pool, no more messages
            console.log(8822);
            if (successCB) {



                console.log(882244);
                successCB(response);
            }



        });
    }




};