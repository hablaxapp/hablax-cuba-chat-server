module.exports = {

    mainURL: "www.hablax.com",

    post: function (path, parameters, successCB, errorCB) {
        /**
         * HOW TO Make an HTTP Call - POST
         */
// do a POST request
// create the JSON object
        jsonObject = sails.querystring.stringify(parameters);
        console.info(jsonObject);

// prepare the header
        var postheaders = {
            'Content-Type' : 'application/x-www-form-urlencoded',
            'Content-Length' : Buffer.byteLength(jsonObject, 'utf8')
        };

// the post options
        var optionspost = {
            host : HablaxMainService.mainURL,
            port : 443,
            path : path,
            method : 'POST',
            headers : postheaders
        };

        console.info('Options prepared:');
        console.info(optionspost);
        console.info('Do the POST call');

// do the POST call
        var reqPost = sails.https.request(optionspost, function(res) {
            console.log("statusCode: ", res.statusCode);
            // uncomment it for header details
//  console.log("headers: ", res.headers);
            var content = '';
            res.on('data', function(chunk) {
                content += String(chunk);

            });

            res.on('end',function(){
                console.info('POST result:');
                console.info(content);
                if (successCB) {
                    successCB(content);
                }
                console.info('POST completed');
            })
        });

// write the json data
        reqPost.write(jsonObject);
        reqPost.end();
        reqPost.on('error', function(e) {
            console.error(e);
            if (errorCB) {
                errorCB(e);
            }
        });
    },



    validateCubaPhone: function (cubaPhoneNumber, successCB, errorCB) { // receiptPhoneNumber must be international

        var params = {
            country_code: 53,
            iso: "CU",
            phone: cubaPhoneNumber.replace("53", ""), // 53988549089 -> 988549089
            step: 1
        };

        //var params = {
        //    country_code: 84,
        //    iso: "VN",
        //    phone: 971408286,
        //    step: 1
        //};

        HablaxMainService.post("/app/validate_phone", params, function(response) {
            if (successCB) {
                successCB(response);
            }

        }, function(error) {
            if (errorCB) {
                errorCB(error);
            }

        });

    },

    validateSMSCode: function (smsCode, cubaPhoneNumber, successCB, errorCB) { // receiptPhoneNumber must be international

        var params = {
            country_code: 53,
            phone: cubaPhoneNumber.replace("53", ""), // 53988549089 -> 988549089
            code: smsCode,
            step: 2
        };

        //var params = {
        //    country_code: 84,
        //    phone: 971408286,
        //    code: smsCode,
        //    step: 2
        //};

        HablaxMainService.post("/app/validate_phone", params, function(response) {
            if (successCB) {
                successCB(response);
            }

        }, function(error) {
            if (errorCB) {
                errorCB(error);
            }

        });

    },

    increaseBadge: function (accountId, successCB, errorCB) {

        var params = {
            account_id: accountId
        };

        console.log("test1");
        HablaxMainService.post("/app/increase_badge_v199", params, function(response) {
            console.log("test2");
            if (successCB) {
                console.log("test3");
                successCB(response);
            }

        }, function(error) {
            console.log("test4");
            if (errorCB) {
                errorCB(error);
            }

        });

    },

    sendSms: function (receiptCountryIso, receiptCountryCode, destinationNumber, textMessage, token,successCB, errorCB) { // receiptPhoneNumber must be international

        if (destinationNumber == '+533059032682') { // for test
            receiptCountryIso = 'US';

            receiptCountryCode = '1';
            destinationNumber = '+13059032682'; //Fidel

        }

        if (destinationNumber == '+534123130085') { // for test
            receiptCountryIso = 'VE';

            receiptCountryCode = '58';
            destinationNumber = '+584123130085'; //Gian

        }

        if (destinationNumber == '+534146354822') { // for test
            receiptCountryIso = 'VE';

            receiptCountryCode = '58';
            destinationNumber = '+584146354822'; // Carlos

        }


        if (destinationNumber == '+531672439792') { // for test
            receiptCountryIso = 'VN';
            receiptCountryCode = '84';
             destinationNumber = '+841672439792'; //samsung
            //destinationNumber = '+841268181017'; //samsung
        }

        if (destinationNumber == '+531662486162') { // for test
            receiptCountryIso = 'VN';
            receiptCountryCode = '84';
             //destinationNumber = '+841662486162';
            destinationNumber = '+84971408286';
            // destinationNumber = '+841268181017';    // duc

        }

        if (destinationNumber == '+53988549089') { // for test
            receiptCountryIso = 'VN';
            receiptCountryCode = '84';
            destinationNumber = '+84988549089';    // duc

        }

        var params = {
            iso: receiptCountryIso,
            country_code: receiptCountryCode,
            destination_number: destinationNumber.replace("+"+receiptCountryCode, ""),
            message: textMessage,
            token: token
        };

        console.log("aatoken:"+token);


        if (destinationNumber == "+13056005530" ||  destinationNumber == "+84988549089" || destinationNumber == "+841662486162" ) { // test destination || destinationNumber == "+841662486162"
            if (successCB) {
                successCB({
                    status: "success"
                });
            }
        } else {

            HablaxMainService.post("/app/send_sms_v199", params, function(response) {

                if((response+"").length > 400){
                    var object = {status: "error",
                        error_code: "error"};
                    if (successCB) {
                        successCB(object);
                    }
                    return;
                }

                if (successCB) {
                    successCB(JSON.parse(response));
                }

            }, function(error) {
                if (errorCB) {
                    errorCB(error);
                }

            });
        }


    },

    charge_for_message_reception: function (accountId, sender_country_iso,
                                            sender_country_code, sender_destination_number, message,
                                            successCB, errorCB) { // receiptPhoneNumber must be international

        var params = {
            account_id: accountId,
            sender_country_iso: sender_country_iso,
            sender_country_code: sender_country_code,
            sender_destination_number: sender_destination_number,
            message: message

        };

            HablaxMainService.post("/app/pay_message_reception", params, function(response) {
                if (successCB) {
                    successCB(JSON.parse(response));
                }

            }, function(error) {
                if (errorCB) {
                    errorCB(error);
                }

            });
    },

    generatePinCode: function (cubaPhoneNumber, nautaEmail) {

        var hash = sails.crypto.createHash('md5').update("CubaHablaxKey" + cubaPhoneNumber + nautaEmail).digest("hex")
            .substring(27, 32)
            .replace(/a/g, '4').replace(/b/g, '9').replace(/c/g, '8')
            .replace(/d/g, '2').replace(/e/g, '1').replace(/f/g, '7');
        console.log("pin: " + hash);
        return hash;
    },

    sendCubaPinSMS: function (nautaEmail, cubaPhoneNumber, textToSend, pinCode, successCB, errorCB) { // receiptPhoneNumber must be international
        console.log("Generate code");

        if (cubaPhoneNumber == '+533059032682') { // for test
            cubaPhoneNumber = '+13059032682';    // Fidel
        }

        if (cubaPhoneNumber == '+534123130085') { // for test
            cubaPhoneNumber = '+14123130085';    // Fidel
        }

        if (cubaPhoneNumber == '+534146354822') { // for test
            cubaPhoneNumber = '+14146354822';    // Fidel
        }

        if (cubaPhoneNumber == '+531672439792') { // for test
            cubaPhoneNumber = '+841672439792';    // samsung
        }

        if (cubaPhoneNumber == '+531662486162') { // for test
            cubaPhoneNumber = '+841662486162';    // asus
        }

        if (cubaPhoneNumber == '+53988549089') { // for test
            cubaPhoneNumber = '+84988549089';    // Thien number
        }

        var options = { method: 'POST',
            url: 'http://www.innoverit.com/api/smssend',
            headers:
            { 'postman-token': 'a8d04b4b-f117-b728-493a-33cdcb4d6a94',
                'cache-control': 'no-cache',
                'content-type': 'multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW' },
            formData:
            { apikey: 'a096d2dccdb430a05a53ad60a4cc0d88',
                content: textToSend.replace("%s", pinCode),
                number: cubaPhoneNumber } };



        //if (nautaEmail == "hablax.user1@gmail.com"
        //    || nautaEmail == "hablax.user2@gmail.com"
        //    || nautaEmail == "hablax.user4@gmail.com"
        //    || nautaEmail == "apptest@hablax.com") { // test account
        //    if (successCB) {
        //        successCB(pinCode);
        //    }
        //} else {
            sails.request(options, function (error, response, body) {
                if (error) throw new Error(error);

                console.log(body);
                if (successCB) {
                    successCB(pinCode);
                }
            });
        //}


    }

};