/**
 * Bootstrap
 * (sails.config.bootstrap)
 *
 * An asynchronous bootstrap function that runs before your Sails app gets lifted.
 * This gives you an opportunity to set up your data model, run jobs, or perform some special logic.
 *
 * For more information on bootstrapping your app, check out:
 * http://sailsjs.org/#!/documentation/reference/sails.config/sails.config.bootstrap.html
 */

module.exports.bootstrap = function(cb) {

  // It's very important to trigger this callback method when you are finished
  // with the bootstrap!  (otherwise your server will never lift, since it's waiting on the bootstrap)
  sails.listener2 = require('mail-listener2');
  //sails.listener2 = require('mail-listener2-updated');

  //sails.listener = require('mail-listener');
  sails.libphonenumber = require('google-libphonenumber');
  sails.https = require('https');
  sails.querystring = require('querystring');
  sails.request = require("request");


  sails.FCM = require('fcm-push');
  sails.nodemailer = require('nodemailer');
  sails.crypto = require('crypto');

  sails.on('lifted', function() {
    var someService = require('../api/services/Emailfetcher.js');
    // Start background process of SomeService.
    someService.startBackgroundProcess();

  });
  cb();
};
