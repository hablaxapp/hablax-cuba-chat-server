CREATE TABLE IF NOT EXISTS `cuba_accounts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `number` char(20) NOT NULL,
  `creation` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `email` char(80) NOT NULL DEFAULT '',
  `deleted` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1=deleted',
  `iso_code` char(3) NOT NULL DEFAULT '',
  `country_code` int(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `number` (`number`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `cuba_chats` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email_user_from` char(255) NOT NULL,
  `email_user_to` char(255) NOT NULL,
  `body` char(255) NOT NULL,
  `subject` char(255) NOT NULL,
  `object` char(255) NOT NULL,
  `createdAt` char(255) NOT NULL,
  `updatedAt` char(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

DROP TABLE `cuba_chats`;

CREATE TABLE IF NOT EXISTS `messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email_user_from` char(255) NOT NULL,
  `email_user_to` char(255) NOT NULL,
  `text_message` char(255) NOT NULL,
  `admin_notes` char(255) NOT NULL,
  `createdAt` char(255) NOT NULL,
  `updatedAt` char(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `rooms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `room_members` char(255) NOT NULL,
  `createdAt` char(255) NOT NULL,
  `updatedAt` char(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

ALTER TABLE `rooms` MODIFY COLUMN room_members TEXT NOT NULL;
ALTER TABLE `rooms` ADD COLUMN recent_message TEXT NOT NULL DEFAULT '';
ALTER TABLE `rooms` ADD COLUMN phone_numbers TEXT NOT NULL;
ALTER TABLE `rooms` ADD COLUMN member_names TEXT NOT NULL;
ALTER TABLE `rooms` ADD COLUMN room_name nvarchar(255);

ALTER TABLE `messages` ADD COLUMN room_id int(11) NOT NULL;

ALTER TABLE `cuba_accounts` ADD COLUMN app_version nvarchar(255);
ALTER TABLE `cuba_accounts` ADD COLUMN device_platform nvarchar(255);
ALTER TABLE `cuba_accounts` ADD COLUMN device_model nvarchar(255);
ALTER TABLE `cuba_accounts` ADD COLUMN device_version nvarchar(255);
ALTER TABLE `cuba_accounts` ADD COLUMN device_uuid nvarchar(255);

ALTER TABLE `cuba_accounts` ADD COLUMN update_at datetime NOT NULL DEFAULT '0000-00-00 00:00:00';

ALTER TABLE `rooms` ADD COLUMN country_code TEXT NOT NULL DEFAULT '';
ALTER TABLE `rooms` ADD COLUMN country_code_iso TEXT NOT NULL DEFAULT '';
ALTER TABLE `rooms` ADD COLUMN receiver_name TEXT NULL DEFAULT NULL;

ALTER TABLE `rooms` DROP COLUMN receiver_name;
ALTER TABLE `cuba_accounts` ADD COLUMN recent_pin char(10);
ALTER TABLE `rooms` ADD COLUMN send_outside TINYINT NULL DEFAULT 0;


CREATE TABLE IF NOT EXISTS `global_sockets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `socket_id` char(20) NOT NULL,
  `global_account_id` int(11) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT 0 COMMENT '1=deleted',
  `createdAt` char(255) NOT NULL,
  `updatedAt` char(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

ALTER TABLE `global_sockets` ADD COLUMN `global_account_token` char(255) NOT NULL;

ALTER TABLE `cuba_accounts` MODIFY COLUMN `update_at` nvarchar(255);
ALTER TABLE `cuba_accounts` MODIFY COLUMN `creation` nvarchar(255);



ALTER TABLE `rooms` ADD COLUMN time_update TEXT NOT NULL DEFAULT '';
ALTER TABLE `rooms` MODIFY COLUMN updatedAt TEXT NOT NULL DEFAULT '';
ALTER TABLE `messages` ADD COLUMN images text NOT NULL DEFAULT '';
ALTER TABLE `messages` ADD COLUMN audio_file text NOT NULL DEFAULT '';
ALTER TABLE `messages` ADD COLUMN track_status char(255) NOT NULL DEFAULT '';
ALTER TABLE `messages`MODIFY COLUMN `track_status`char(255) NOT NULL DEFAULT '' COMMENT '11:delivered; 12:seen; 13:failed';
ALTER TABLE `messages`MODIFY COLUMN `track_status` int(11) NOT NULL DEFAULT 11;
ALTER TABLE `global_sockets` DROP COLUMN `global_account_id`;
ALTER TABLE `global_sockets` ADD COLUMN `global_account_email` char(255) NOT NULL;

ALTER TABLE `messages`MODIFY COLUMN `track_status` int(11) NOT NULL DEFAULT 11 COMMENT '11:delivered; 12:seen; 13:failed';
ALTER TABLE `rooms` ADD COLUMN `pending` int(11) NOT NULL DEFAULT 0;
ALTER TABLE `rooms`MODIFY COLUMN `pending` int(11) NOT NULL DEFAULT 0 COMMENT '0 is not pending, 1 is pending';

CREATE TABLE IF NOT EXISTS `server_email` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` char(255) NOT NULL,
  PRIMARY KEY (`id`)

) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

ALTER TABLE `rooms` ADD COLUMN `visible_with_user` nvarchar(255) NOT NULL DEFAULT '1,1' COMMENT '0 is not visible, 1 is visible';

ALTER TABLE `messages` ADD COLUMN `visible_with_user` nvarchar(255) NOT NULL DEFAULT '1,1' COMMENT '0 is not visible, 1 is visible';

INSERT INTO `server_email`(`id`, `email`) VALUES (1,'hablax.user1@gmail.com');
INSERT INTO `server_email`(`id`, `email`) VALUES (2,'hablax.user2@gmail.com');
INSERT INTO `server_email`(`id`, `email`) VALUES (3,'hablax.user4@gmail.com');

ALTER TABLE `messages` ADD COLUMN `order_in_room` int(11) NOT NULL DEFAULT 0;
