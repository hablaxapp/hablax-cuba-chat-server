CREATE VIEW search_chat AS
SELECT *,concat('+',`country_code`,`telephone_1`) as formattedPhoneNumber
FROM accounts;


#DO THIS IN MAIN DATABASE
ALTER TABLE `save_transaction` ADD COLUMN `sms_transport` char(20) NULL DEFAULT 'SEND';
ALTER TABLE `sms_logs` ADD COLUMN `sms_transport` char(20) NULL DEFAULT 'SEND';
ALTER TABLE `token_push` ADD COLUMN `badge` int(11) NULL DEFAULT 0;
ALTER TABLE `token_push` ADD COLUMN `accountToken` varchar(45) NULL;